export const escapeRegExp = (value) => {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

export const searchHandler = (searchValue, tableRows, setSearchText, setRows) => {
    setSearchText(searchValue);
    const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
    const filteredRows = tableRows.filter((row) => {
        return Object.keys(row).some((field) => {
            return searchRegex.test(row[field].toString());
        });
    });
    setRows(filteredRows);
};
