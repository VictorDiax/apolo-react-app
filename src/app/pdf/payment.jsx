import React from 'react';
import { format } from 'date-fns';
import { Document, Page, Text, View, StyleSheet, Image } from '@react-pdf/renderer';
import Logo from '../assets/images/apolo11.png';

const Payment = (props) => {
    const { agreement, payments, debt, client, total, currentDebt, documentId, months, payed } = props.item;
    let date = new Date();

    const styles = StyleSheet.create({
        page: {
            backgroundColor: '#fff'
        },
        main: {
            margin: 5
        },
        header: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            margin: 10
        },
        logo: {
            width: 70,
            height: 70
        },
        headerTitleContainer: {
            width: '90%',
            textAlign: 'center',
        },
        headerTitle: {
            fontSize: 12,
            marginBottom: 5,
            color: '#318ce7'
        },
        headerText: {
            fontSize: 12,
            marginBottom: 5
        },
        subHeader: {
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-around'
        },
        body: {
            marginLeft: 20,
            marginRight: 20
        },
        title: {
            backgroundColor: '#318ce7',
            padding: 4,
            fontSize: 12,
            color: 'white',
            margin: 10,
        },
        tableContent: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 5
        },
        th: {
            width: '20%',
            marginBottom: 10
        },
        thText: {
            fontSize: 12,
            color: '#318ce7'
        },
        tr: {
            width: '20%',
        },
        detail: {
            fontSize: 10,
            margin: 5
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 5
        },
        text: {
            fontSize: 12,
        },
        totalContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '#318ce7',
            padding: 4,
            margin: 10,
        },
        totalText: {
            fontSize: 12,
            color: 'white'
        },
        date: {
            fontSize: 12,
            margin: 20,
            position: 'absolute',
            bottom: 0,
            right: 0,
            color: 'gray'
        }
    })


    return (
        <Document>
            <Page size='A4' style={styles.page}>
                <View style={styles.main}>
                    <View style={styles.header}>
                        <Image style={styles.logo} src={{ uri: Logo }} />
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>EMPRESA DE TELEVISIÓN POR CABLE</Text>
                            <Text style={styles.headerText}> TV Cable Acoyapa Apolo 11</Text>
                            <Text style={styles.headerText}>Acoyapa, Chontales</Text>
                        </View>
                    </View>
                    <Text style={{ ...styles.detail, fontSize: 12, color: '#318ce7', textAlign: 'center', margin: 25 }}>
                        {`Factura N°: ${documentId}`}
                    </Text>
                    <Text style={styles.detail}>{`Contrato N°: ${agreement}`}</Text>
                    <Text style={{ ...styles.detail, marginBottom: 15 }}>{`Fecha de emisión: ${payed}`}</Text>
                    <Text style={styles.title}>RESUMEN DE LA CUENTA</Text>
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <Text style={styles.thText}>Cliente</Text>
                            <Text style={styles.text}>{client}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.thText}>Meses facturados</Text>
                            <Text style={styles.text}>{payments}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.thText}>Saldo anterior C$</Text>
                            <Text style={styles.text}>{debt}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.thText}>Saldo actual C$</Text>
                            <Text style={styles.text}>{currentDebt}</Text>
                        </View>
                    </View>
                    <Text style={styles.title}>DETALLE DE FACUTURA</Text>
                    <View style={styles.tableContent}>
                        <View style={styles.th}><Text style={styles.thText}>Mes</Text></View>
                        <View style={styles.th}><Text style={styles.thText}>Año</Text></View>
                        <View style={styles.th}><Text style={styles.thText}>Costo C$</Text></View>
                        <View style={styles.th}><Text style={styles.thText}>Conexiones</Text></View>
                        <View style={{ ...styles.th, textAlign: 'right' }}><Text style={styles.thText}>Total C$</Text></View>
                    </View>
                    {
                        months && months.map((el) => {
                            return (
                                <View key={el.id} style={styles.tableContent}>
                                    <View style={styles.tr}>
                                        <Text style={styles.text}>{el.month}</Text>
                                    </View>
                                    <View style={styles.tr}>
                                        <Text style={styles.text}>{el.year}</Text>
                                    </View>
                                    <View style={styles.tr}>
                                        <Text style={styles.text}>{el.unitPrice}</Text>
                                    </View>
                                    <View style={styles.tr}>
                                        <Text style={styles.text}>{el.connection}</Text>
                                    </View>
                                    <View style={{ ...styles.tr, textAlign: 'right' }}>
                                        <Text style={styles.text}>{el.total}</Text>
                                    </View>
                                </View>
                            )
                        })
                    }
                    <View style={styles.totalContainer}>
                        <Text style={styles.totalText}>TOTAL A PAGAR C$</Text>
                        <Text style={styles.totalText}>{total}</Text>
                    </View>
                </View>
                <Text style={styles.date}>{format(date, 'dd/MM/yyyy')}</Text>
            </Page>
        </Document >
    )
}

export default Payment;
