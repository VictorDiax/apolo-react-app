import React from 'react';
import { format } from 'date-fns';
import { Document, Page, Text, View, StyleSheet, Image } from '@react-pdf/renderer';
import Logo from '../assets/images/apolo11.png';

const Agreement = (props) => {
    const { number, idCard, name, lastName, address, phone, locationName,
        service, price, created, state
    } = props.item;
    let date = new Date();

    const styles = StyleSheet.create({
        page: {
            backgroundColor: '#fff'
        },
        main: {
            margin: 5
        },
        header: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            margin: 10
        },
        logo: {
            width: 70,
            height: 70
        },
        headerTitleContainer: {
            width: '90%',
            textAlign: 'center',
        },
        headerTitle: {
            fontSize: 12,
            marginBottom: 5,
            color: '#318ce7'
        },
        headerText: {
            fontSize: 12,
            marginBottom: 5
        },
        subHeader: {
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-around'
        },
        body: {
            marginLeft: 20,
            marginRight: 20
        },
        title: {
            backgroundColor: '#318ce7',
            padding: 4,
            fontSize: 12,
            color: 'white',
            margin: 10,
        },
        detail: {
            fontSize: 10,
            textAlign: 'center',
            color: '#318ce7',
            margin: 10
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 5
        },
        subtitle: {
            fontSize: 12,
            color: '#318ce7'
        },
        text: {
            fontSize: 12,
        },
        date: {
            fontSize: 12,
            margin: 20,
            position: 'absolute',
            bottom: 0,
            right: 0,
            color: 'gray'
        }
    })


    return (
        <Document>
            <Page size='A4' style={styles.page}>
                <View style={styles.main}>
                    <View style={styles.header}>
                        <Image style={styles.logo} src={{ uri: Logo }} />
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>EMPRESA DE TELEVISIÓN POR CABLE</Text>
                            <Text style={styles.headerText}> TV Cable Acoyapa Apolo 11</Text>
                            <Text style={styles.headerText}>Acoyapa, Chontales</Text>
                        </View>
                    </View>
                    <Text style={styles.detail}>{`Contrato N°: ${number}`}</Text>
                    <Text style={styles.title}>DETALLE DEL CLIENTE</Text>
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Cédula de identidad</Text>
                            <Text style={styles.text}>{idCard}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Nombre</Text>
                            <Text style={styles.text}>{name}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Apellido</Text>
                            <Text style={styles.text}>{lastName}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Dirección</Text>
                            <Text style={styles.text}>{address}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Localidad</Text>
                            <Text style={styles.text}>{locationName}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Teléfono</Text>
                            <Text style={styles.text}>{phone}</Text>
                        </View>
                    </View>
                    <Text style={styles.title}>DETALLE DEL SERVICIO</Text>
                    <View style={styles.body}>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Descripción</Text>
                            <Text style={styles.text}>{service}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Costo C$</Text>
                            <Text style={styles.text}>{price}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Apertura</Text>
                            <Text style={styles.text}>{created && format(created.toDate(), 'dd/MM/yyyy')}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.subtitle}>Estado</Text>
                            <Text style={styles.text}>{state}</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.date}>{format(date, 'dd/MM/yyyy')}</Text>
            </Page>
        </Document >
    )
}

export default Agreement;
