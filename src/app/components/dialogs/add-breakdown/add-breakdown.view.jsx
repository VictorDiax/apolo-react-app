import * as React from 'react';
import {
    Typography, TextField,
    IconButton, Dialog, RadioGroup,
    InputAdornment, DialogActions,
    DialogContent, Grid, DialogTitle,
    FormLabel, FormControl, FormControlLabel,
    Radio
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import PlaceOutlinedIcon from '@mui/icons-material/PlaceOutlined';
import Toast from '../../toast/toast';
import Transition from '../transition/transition';

const stateOps = [
    { key: 1, label: "Reportada", color: "warning" },
    { key: 2, label: "En proceso", color: "primary" },
    { key: 3, label: "Reparada", color: "success" }
];

const AddAgreementView = (props) => {
    const {
        open, handleClickOpen, handleChange, values,
        saveHandler, dialogType, severity, message, openSnack,
        handleCloseSnack, user
    } = props;

    return (
        <>
            <Dialog maxWidth='md' TransitionComponent={Transition} open={open} onClose={() => handleClickOpen(false)}>
                <DialogTitle><Typography color="primary">
                    {dialogType === 0 ?
                        "Nueva avería" :
                        (dialogType === 1 || dialogType === 6 ? "Editar avería"
                            : "Eliminar avería")}
                </Typography></DialogTitle>
                <DialogContent dividers>
                    <Grid container spacing={2}>
                        <Grid marginBottom={4} item xs={12} sm={12} md={12} lg={12}>
                            <TextField
                                id="address-field"
                                variant="standard"
                                focused
                                multiline
                                label="Dirección"
                                disabled={true}
                                value={user ? user.address : values.address}
                                style={{ width: '100%' }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <PlaceOutlinedIcon color='primary' />
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid marginBottom={4} item xs={12} sm={12} md={12} lg={12}>
                            <TextField
                                id="desc-field"
                                variant="standard"
                                focused
                                multiline
                                label="Descripción"
                                disabled={dialogType === 2 || dialogType === 6 ? true : false}
                                value={values.description}
                                onChange={handleChange('description')}
                                style={{ width: '100%' }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <HelpOutlineIcon color='primary' />
                                        </InputAdornment>
                                    )
                                }}
                                error={values.descriptionError}
                                helperText={values.descriptionError && "Campo obligatorio"}
                            />
                        </Grid>
                        {
                            dialogType === 6 &&
                            <Grid marginBottom={4} item xs={12} sm={12} md={12} lg={12}>
                                <FormControl component="fieldset">
                                    <FormLabel style={{ fontSize: 13, color: '#1976d2', marginBottom: 4 }} component="legend">Estado</FormLabel>
                                    <RadioGroup
                                        row
                                        aria-label="state"
                                        name="row-radio-buttons-group"
                                        value={values.state}
                                        onChange={handleChange('state')}
                                    >
                                        {
                                            stateOps.map((item) => {
                                                return (
                                                    <FormControlLabel key={item.key} value={item.key} control={<Radio color={item.color} />} label={item.label} />
                                                )
                                            })
                                        }
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        }
                    </Grid>
                </DialogContent >
                <DialogActions style={{ display: 'flex', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                    <IconButton
                        onClick={() => handleClickOpen(false)}
                        title="Cancelar"
                        aria-label="Cancelar"
                        size="small"
                        style={{ marginRight: 10 }}
                    >
                        <CloseIcon color='primary' fontSize="medium" />
                    </IconButton>
                    <IconButton
                        onClick={saveHandler}
                        title="Guardar"
                        aria-label="Guardar"
                        size="small"
                    >
                        <SaveIcon color='primary' fontSize="medium" />
                    </IconButton>
                </DialogActions>
            </Dialog >
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
        </>
    );
}

export default AddAgreementView;
