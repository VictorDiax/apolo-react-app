import React from 'react'
import AddBreakdownVw from './add-breakdown.view';
import { createBreakdown, updateBreakdown, deleteBreakdown } from '../../../../lib/data/breakdowns';

const AddBreakdown = (props) => {
    const {
        open,
        handleClickOpen,
        loadData,
        dialogType,
        values,
        setValues,
        handleSelection,
        selectedRow,
        currentUser
    } = props;
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };

    const validate = () => {
        const { description } = values;
        let descriptionError = false;

        if (description === '') descriptionError = true;
        else descriptionError = false;

        setValues({ ...values, descriptionError });

        if (!descriptionError) return false;
        else return true;
    }

    const saveHandler = () => {
        const error = validate();
        if (!error) {
            switch (dialogType) {
                case 0:
                    createBreakdown({ description: values.description, agreement: currentUser.id }).then(() => {
                        handleClickSnack('success', 'La avería ha sido registrada de forma exitosa!'); resetForm();
                    });
                    break;
                case 1:
                    updateBreakdown({ description: values.description }, selectedRow.documentId).then(() => {
                        handleClickSnack('info', 'La avería ha sido actualizada de forma exitosa!'); resetForm();
                    });
                    break;
                case 2:
                    deleteBreakdown(selectedRow.documentId).then(() => { handleClickSnack('error', 'La avería ha sido eliminada de forma exitosa!'); resetForm(); });
                    break;
                case 6:
                    updateBreakdown({ state: parseInt(values.state) }, selectedRow.documentId).then(() => {
                        handleClickSnack('info', 'La avería ha sido actualizada de forma exitosa!'); resetForm();
                    });
                    break;
                default:
                    break;
            }
        }
    }

    const resetForm = () => {
        handleClickOpen(false);
        loadData();
        handleSelection({ id: selectedRow.id });
    }

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    return (
        <AddBreakdownVw
            open={open}
            handleClickOpen={handleClickOpen}
            handleChange={handleChange}
            values={values}
            saveHandler={saveHandler}
            dialogType={dialogType}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
            user={currentUser}
        />
    )
}

export default AddBreakdown;
