import * as React from 'react';
import {
    Typography, IconButton, Dialog,
    DialogActions, DialogContent, Grid, DialogTitle,
    Card, CardHeader, CardContent, List, ListItem,
    ListItemText, Table, TableBody, TableCell, TableContainer,
    TableHead, TableRow, Checkbox, Box
} from '@mui/material';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import Toast from '../../toast/toast';
import Transition from '../transition/transition';

const EnhancedTableHead = (props) => {
    const { onSelectAllClick, numSelected, headCells, rowCount } = props;

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts',
                        }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell key={headCell.id}>
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const PaymentsPointVw = (props) => {
    const {
        open, handleClickOpen, saveHandler,
        severity, message, openSnack,
        handleCloseSnack, data, headCells, rows,
        selected, isSelected, date, handleSelectAllClick,
        handleClick
    } = props;

    return (
        <>
            <Dialog maxWidth='md' TransitionComponent={Transition} open={open} onClose={() => handleClickOpen(false)}>
                <DialogTitle><Typography color="primary">Pago de servicio</Typography></DialogTitle>
                <DialogContent dividers>
                    <Grid spacing={1} container>
                        <Grid display='flex' justifyContent='end' item xs={12} sm={12} md={12} lg={12}>
                            <Typography variant='body2' color="primary">Fecha de emisión: {date}</Typography>
                        </Grid>
                        <Grid item xs={12} sm={12} md={6} lg={6}>
                            <Card>
                                <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 14, color: 'rgb(0, 114, 229)' } }} title="RESUMEN DE CUENTA" />
                                <CardContent>
                                    <List sx={{ p: 0 }}>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={data.connection} />}>
                                            <ListItemText primary="N° de conexiones" />
                                        </ListItem>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={data.pending} />}>
                                            <ListItemText primary="Facturas pendientes" />
                                        </ListItem>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={rows.reduce((sum, current) => sum + current.total, 0)} />}>
                                            <ListItemText primary="Saldo actual" />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={6} lg={6}>
                            <Card>
                                <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 14, color: 'rgb(0, 114, 229)' } }} title="INFORMACIÓN DEL CLIENTE" />
                                <CardContent>
                                    <List sx={{ p: 0 }}>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={data.number} />}>
                                            <ListItemText primary="N° de contrato" />
                                        </ListItem>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={data.name} />}>
                                            <ListItemText primary="Nombre" />
                                        </ListItem>
                                        <ListItem sx={{ p: 0 }} secondaryAction={<ListItemText secondary={data.lastName} />}>
                                            <ListItemText primary="Apellidos" />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Card>
                                <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 14, color: 'rgb(0, 114, 229)' } }} title="MESES A CARGAR" />
                                <CardContent>
                                    <Grid container>
                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                            <TableContainer>
                                                <Table
                                                    sx={{ minWidth: 750 }}
                                                    aria-labelledby="tableTitle"
                                                    size={'small'}
                                                >
                                                    <EnhancedTableHead
                                                        numSelected={selected.length}
                                                        onSelectAllClick={handleSelectAllClick}
                                                        headCells={headCells}
                                                        rowCount={rows.length}
                                                    />
                                                    <TableBody>
                                                        {rows.map((row, index) => {
                                                            const isItemSelected = isSelected(row.documentId);
                                                            const labelId = `enhanced-table-checkbox-${index}`;

                                                            return (
                                                                <TableRow
                                                                    hover
                                                                    onClick={(event) => handleClick(event, row.documentId)}
                                                                    role="checkbox"
                                                                    aria-checked={isItemSelected}
                                                                    tabIndex={-1}
                                                                    key={row.id}
                                                                    selected={isItemSelected}
                                                                >
                                                                    <TableCell padding="checkbox">
                                                                        <Checkbox
                                                                            color="primary"
                                                                            checked={isItemSelected}
                                                                            inputProps={{
                                                                                'aria-labelledby': labelId,
                                                                            }}
                                                                        />
                                                                    </TableCell>
                                                                    <TableCell
                                                                        component="th"
                                                                        id={labelId}
                                                                        scope="row"
                                                                    >
                                                                        {row.id}
                                                                    </TableCell>
                                                                    <TableCell >{row.month}</TableCell>
                                                                    <TableCell>{row.year}</TableCell>
                                                                    <TableCell>{row.unitPrice}</TableCell>
                                                                    <TableCell>{row.connection}</TableCell>
                                                                    <TableCell>{row.total}</TableCell>
                                                                </TableRow>
                                                            );
                                                        })}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                            <Box display='flex' mt={2} alignItems='center' justifyContent='space-between'>
                                                <Typography variant='subtitle2' color="primary">TOTAL C$</Typography>
                                                <Typography variant='subtitle2' color="primary">{rows.filter((x) => { return selected.includes(x.documentId) }).reduce((sum, current) => sum + current.total, 0)}</Typography>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions style={{ display: 'flex', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                    <IconButton
                        onClick={() => handleClickOpen(false)}
                        title="Cancelar"
                        aria-label="Cancelar"
                        size="small"
                        style={{ marginRight: 10 }}
                    >
                        <CloseIcon color='primary' fontSize="medium" />
                    </IconButton>
                    <IconButton
                        onClick={saveHandler}
                        title="Guardar"
                        aria-label="Guardar"
                        size="small"
                    >
                        <SaveIcon color='primary' fontSize="medium" />
                    </IconButton>
                </DialogActions>
            </Dialog>
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
        </>
    );
}

export default PaymentsPointVw;
