import React from 'react'
import PaymentsPointVw from './payments-point.view';
import { format } from 'date-fns';
import { loadPayments } from '../../../../lib/data/payments';

const headCells = [
    {
        id: 'id',
        disablePadding: true,
        label: '#',
    },
    {
        id: 'month',
        disablePadding: false,
        label: 'Mes',
    },
    {
        id: 'year',
        disablePadding: false,
        label: 'Año',
    },
    {
        id: 'unitPrice',
        disablePadding: false,
        label: 'Costo C$',
    },
    {
        id: 'connection',
        disablePadding: false,
        label: 'Conexiones',
    },
    {
        id: 'total',
        disablePadding: false,
        label: 'Total C$',
    }
];

const PaymentsPoint = (props) => {
    const {
        open, data, handleOpenPoint, handleState, loadData, rows
    } = props;
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');
    const [selected, setSelected] = React.useState([]);
    const isSelected = (id) => selected.indexOf(id) !== -1;
    const date = format(new Date(), 'dd/MM/yyyy');

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            setSelected(rows.map((n) => n.documentId));
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };

    const saveHandler = () => {
        let toPay = rows.filter((x) => { return selected.includes(x.documentId) });
        if (toPay.length > 0) {
            loadPayments(toPay, data, rows.reduce((sum, current) => sum + current.total, 0));
            handleClickSnack('success', 'El pago se ha realizado de forma exitosa!');
            resetForm();
        }
        else {
            handleClickSnack('error', 'No se ha seleccionado ni un mes a pagar!');
        }
    }

    const resetForm = () => {
        handleState(data.number);
        loadData();
        handleOpenPoint(false);
        setSelected([]);
    }

    return (
        <PaymentsPointVw
            open={open}
            handleClickOpen={handleOpenPoint}
            saveHandler={saveHandler}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
            data={data}
            headCells={headCells}
            rows={rows}
            selected={selected}
            isSelected={isSelected}
            date={date}
            handleSelectAllClick={handleSelectAllClick}
            handleClick={handleClick}
        />
    )
}

export default PaymentsPoint;
