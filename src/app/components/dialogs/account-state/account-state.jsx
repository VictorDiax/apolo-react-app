import React from 'react'
import AccountStateVw from './account-state.view';
import { searchHandler } from '../../../../utils/functions';

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "total", headerName: "C$ Valor", width: 250 },
    { field: "month", headerName: "Mes", width: 250 },
    { field: "year", headerName: "Año", width: 250 },
    { field: "topayed", headerName: "Fecha pago", width: 200 },
    { field: "payedDate", headerName: "Fecha cancelación", width: 200 },
    { field: "ispayed", headerName: "Estado", width: 150, type: "boolean" }
];

const AccountState = (props) => {
    const {
        open, handleClickOpen, isLoading,
        detailInfo, detailRws, setDetailRws,
        handleState, client, loadData
    } = props;
    const [searchText, setSearchText] = React.useState('');
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [openPoint, setOpenPoint] = React.useState(false);

    const handleOpenPoint = (value) => {
        setOpenPoint(value);
    }

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, detailRws, setSearchText, setDetailRws);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
            handleClickOpen(true);
        }
    }


    return (
        <AccountStateVw
            open={open}
            handleClickOpen={handleClickOpen}
            rows={detailRws}
            detail={detailInfo}
            handleState={handleState}
            loadData={loadData}
            searchText={searchText}
            requestSearch={requestSearch}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            columns={columns}
            isLoading={isLoading}
            isCrud={false}
            data={client}
            openPoint={openPoint}
            handleOpenPoint={handleOpenPoint}
        />
    )
}

export default AccountState;
