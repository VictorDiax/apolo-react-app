import * as React from 'react';
import {
    Dialog, AppBar, Toolbar, IconButton,
    Typography, Slide, Box
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import DataGrid from '../../data-grid/data-grid';
import PaymentsPoint from '../payments-point/payments-point';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const AccountStateVw = (props) => {
    const {
        open, handleClickOpen, rows,
        searchText, requestSearch,
        pageSize, setPageSize, detail,
        selectionModel, handleSelection,
        columns, isLoading, isCrud, data,
        openPoint, handleOpenPoint, loadData,
        handleState
    } = props;

    return (
        <Dialog
            fullScreen
            open={open}
            onClose={() => handleClickOpen(false)}
            TransitionComponent={Transition}
        >
            <AppBar sx={{ position: 'relative' }}>
                <Toolbar style={{ background: 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(164,38,199,1) 0.1%, rgba(65,220,255,1) 90.1% )' }}>
                    <IconButton
                        edge="start"
                        color="inherit"
                        onClick={() => handleClickOpen(false)}
                        aria-label="close"
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                        Estado de cuenta
                    </Typography>
                    <IconButton
                        color="inherit"
                        onClick={() => handleOpenPoint(true)}
                        title="Caja"
                        aria-label="Caja"
                    >
                        <PointOfSaleIcon fontSize="large" />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <div style={{ padding: 20 }}>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', pb: 1 }}>
                    <Typography variant="h6" gutterBottom component="div">Cliente: {data.client}</Typography>
                    <Typography color='red' variant="subtitle1" gutterBottom component="div">Meses pendientes: {detail.pending}</Typography>
                    <Typography color='red' variant="subtitle1" gutterBottom component="div">Total: {detail.debt}</Typography>
                </Box>
                <PaymentsPoint
                    open={openPoint}
                    rows={rows.filter(x => x.ispayed === false)}
                    data={data}
                    loadData={loadData}
                    handleState={handleState}
                    handleOpenPoint={handleOpenPoint}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={isCrud}
                    exFunction={false}
                />
            </div>
        </Dialog>
    );
}

export default AccountStateVw;
