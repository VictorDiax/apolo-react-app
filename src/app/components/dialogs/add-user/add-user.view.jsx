import * as React from 'react';
import {
    Typography, TextField,
    IconButton, Dialog, InputAdornment,
    DialogActions, DialogContent, Grid, DialogTitle,
} from '@mui/material';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import Toast from '../../toast/toast';
import Transition from '../transition/transition';

const AddUserVw = (props) => {
    const {
        open,
        handleClickOpen,
        handleChange,
        values,
        saveHandler,
        severity,
        message,
        openSnack,
        handleCloseSnack
    } = props;

    return (
        <>
            <Dialog maxWidth='md' TransitionComponent={Transition} open={open} onClose={() => handleClickOpen(false)}>
                <DialogTitle><Typography color="primary">Cuenta de usuario</Typography></DialogTitle>
                <DialogContent dividers>
                    <Grid padding={4} container spacing={2}>
                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                            <TextField
                                id="email-field"
                                variant="standard"
                                focused
                                label="Correo electrónico"
                                value={values.email}
                                onChange={handleChange('email')}
                                style={{ width: 230 }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <EmailOutlinedIcon color='primary' />
                                        </InputAdornment>
                                    )
                                }}
                                error={values.emailError > 0}
                                helperText={values.emailError > 0 && (values.emailError === 1 ? "Campo obligatorio" : "Valor con formato inválido")}
                            />
                        </Grid>
                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                            <TextField
                                id="pass-field"
                                variant="standard"
                                focused
                                type='password'
                                label="Contraseña"
                                value={values.password}
                                onChange={handleChange('password')}
                                style={{ width: 230 }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockOutlinedIcon color='primary' />
                                        </InputAdornment>
                                    )
                                }}
                                error={values.passwordError > 0}
                                helperText={values.passwordError > 0 && (values.passwordError === 1 ? "Campo obligatorio" : "Valor debe contener al menos 6 caracteres")}
                            />
                        </Grid>
                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                            <TextField
                                id="confirm-field"
                                variant="standard"
                                focused
                                type='password'
                                label="Confirmar contraseña"
                                value={values.confirm}
                                onChange={handleChange('confirm')}
                                style={{ width: 230 }}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockOutlinedIcon color='primary' />
                                        </InputAdornment>
                                    )
                                }}
                                error={values.confirmError > 0}
                                helperText={values.confirmError > 0 && (values.confirmError === 1 ? "Campo obligatorio" : "Valor no coincide con la contraseña")}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions style={{ display: 'flex', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                    <IconButton
                        onClick={() => handleClickOpen(false)}
                        title="Cancelar"
                        aria-label="Cancelar"
                        size="small"
                        style={{ marginRight: 10 }}
                    >
                        <CloseIcon color='primary' fontSize="medium" />
                    </IconButton>
                    <IconButton
                        onClick={saveHandler}
                        title="Guardar"
                        aria-label="Guardar"
                        size="small"
                    >
                        <SaveIcon color='primary' fontSize="medium" />
                    </IconButton>
                </DialogActions>
            </Dialog>
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
        </>
    );
}

export default AddUserVw;
