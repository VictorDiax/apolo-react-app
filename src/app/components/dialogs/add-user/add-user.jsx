import React from 'react'
import AddUserVw from './add-user.view';
import { createUser, updateAuth } from '../../../../lib/data/user';


const AddUser = (props) => {
    const {
        open, handleClickOpen, selectedRow,
        values, initialVals, setValues,
        loadData, dialogType
    } = props;
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };


    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const validate = () => {
        const { email, password, confirm } = values;

        let emailError = 0, passwordError = 0, confirmError = 0;

        if (email === '') emailError = 1;
        else if (!email.includes('@') || !email.includes('.')) emailError = 2;
        else emailError = 0;

        if (password === '') passwordError = 1;
        else if (password.length < 6) passwordError = 2;
        else passwordError = 0;

        if (confirm === '') confirmError = 1;
        else if (confirm !== password) confirmError = 2;
        else confirmError = 0;

        setValues({ ...values, emailError, passwordError, confirmError });
        if (emailError === 0 && passwordError === 0 && confirmError === 0) return false;
        else return true;
    }

    const saveHandler = () => {
        const error = validate();
        if (!error) {
            if (selectedRow.hasUser)
                updateAuth(values.email, values.password, selectedRow.user).then(() => {
                    handleClickSnack('success', 'El usuario ha sido actualizado de forma exitosa!');
                    loadData();
                    handleClickOpen(false);
                    setValues(initialVals);
                }).catch((e) => { console.log(e) });
            else
                createUser({ type: 2, ...values, agree: selectedRow.number }).then(() => {
                    handleClickSnack('success', 'El usuario ha sido creado de forma exitosa!');
                    loadData();
                    handleClickOpen(false);
                    setValues(initialVals);
                }).catch((e) => { console.log(e) });
        }
    }

    return (
        <AddUserVw
            open={open}
            handleClickOpen={handleClickOpen}
            handleChange={handleChange}
            dialogType={dialogType}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
            values={values}
            saveHandler={saveHandler}
        />
    )
}

export default AddUser;
