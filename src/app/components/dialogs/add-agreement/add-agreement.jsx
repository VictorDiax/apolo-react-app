import React from 'react'
import AddAgreementView from './add-agreement.view';
import { checkPaymentsByClient } from '../../../../lib/data/payments';
import { createAgreement, updateAgreement, deleteAgreement } from '../../../../lib/data/agreement';

export const AddAgreement = (props) => {
    const {
        open,
        handleClickOpen,
        loadData,
        dialogType,
        values,
        setValues,
        handleSelection,
        selectedRow
    } = props;
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };

    const validate = () => {
        const { name,
            lastName,
            idCard,
            address,
            phone,
            location,
            connect,
            created
        } = values;
        let nameError = false;
        let lastNameError = false;
        let idCardError = false;
        let addressError = false;
        let phoneError = false;
        let locationError = false;
        let connectError = false;
        let createdError = false;

        if (name === '') nameError = true;
        else nameError = false;

        if (lastName === '') lastNameError = true;
        else lastNameError = false;

        if (idCard === '') idCardError = true;
        else idCardError = false;

        if (address === '') addressError = true;
        else addressError = false;

        if (phone === '') phoneError = true;
        else phoneError = false;

        if (location === '') locationError = true;
        else locationError = false;

        if (connect === '') connectError = true;
        else connectError = false;

        if (created === null) createdError = true;
        else createdError = false;

        setValues({
            ...values,
            nameError,
            lastNameError,
            idCardError,
            addressError,
            phoneError,
            locationError,
            connectError,
            createdError
        });

        if (!nameError && !lastNameError && !idCardError && !addressError &&
            !phoneError && !locationError && !connectError && !createdError) return false;
        else return true;
    }

    const saveHandler = () => {
        const error = validate();
        if (!error) {
            switch (dialogType) {
                case 0:
                    createAgreement({
                        name: values.name, lastName: values.lastName, idCard: values.idCard,
                        address: values.address, phone: values.phone, location: values.location,
                        connection: values.connect, isactive: values.isactive, created: values.created
                    }).then(() => { handleClickSnack('success', 'El contrato ha sido creado de forma exitosa!'); resetForm(); });
                    break;
                case 1:
                    updateAgreement({
                        name: values.name, lastName: values.lastName, idCard: values.idCard,
                        address: values.address, phone: values.phone, location: values.location,
                        connection: values.connect, isactive: values.isactive, created: values.created
                    }, values.id).then(() => { handleClickSnack('info', 'El contrato ha sido actualizado de forma exitosa!'); resetForm(); });
                    break;
                case 2:
                    checkPaymentsByClient(values.id).then((hasPayments) => {
                        if (hasPayments) {
                            handleClickSnack('error', 'El contrato no puede ser eliminado porque el cliente ya posee un estado de cuenta!');
                        } else {
                            deleteAgreement(values.id).then(() => { handleClickSnack('error', 'El contrato ha sido eliminado de forma exitosa!'); resetForm(); });
                        }
                    })
                    break;
                default:
                    break;
            }
        }
    }

    const resetForm = () => {
        loadData();
        handleClickOpen(false);
        handleSelection({ id: selectedRow.id });
    }

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleDateChange = (newLocale) => {
        setValues({ ...values, created: newLocale });
    };

    return (
        <AddAgreementView
            open={open}
            handleClickOpen={handleClickOpen}
            handleChange={handleChange}
            values={values}
            saveHandler={saveHandler}
            dialogType={dialogType}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
            handleDateChange={handleDateChange}
        />
    )
}
