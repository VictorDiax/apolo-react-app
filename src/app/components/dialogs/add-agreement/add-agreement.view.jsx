import * as React from 'react';
import {
    Typography, TextField,
    IconButton, MenuItem,
    Radio, RadioGroup,
    FormControlLabel, FormControl,
    FormLabel, Dialog,
    InputAdornment, DialogActions,
    DialogContent, Grid, DialogTitle,
    Card, CardHeader, CardContent
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import esLocale from 'date-fns/locale/es';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import SegmentIcon from '@mui/icons-material/Segment';
import BrandingWatermarkOutlinedIcon from '@mui/icons-material/BrandingWatermarkOutlined';
import PlaceOutlinedIcon from '@mui/icons-material/PlaceOutlined';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import MapOutlinedIcon from '@mui/icons-material/MapOutlined';
import ConnectedTvOutlinedIcon from '@mui/icons-material/ConnectedTvOutlined';
import InputMask from 'react-input-mask';
import Toast from '../../toast/toast';
import Transition from '../transition/transition';

const AddAgreementView = (props) => {
    const {
        open,
        handleClickOpen,
        handleChange,
        values,
        saveHandler,
        dialogType,
        severity,
        message,
        openSnack,
        handleCloseSnack,
        handleDateChange
    } = props;

    return (
        <>
            <Dialog maxWidth='md' TransitionComponent={Transition} open={open} onClose={() => handleClickOpen(false)}>
                <DialogTitle><Typography color="primary">
                    {dialogType === 0 ?
                        "Nuevo contrato" :
                        (dialogType === 1 ? "Editar contrato"
                            : "Eliminar contrato")}
                </Typography></DialogTitle>
                <DialogContent dividers>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Card>
                                <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)', fontWeight: 'bold' } }} title="Datos personales" />
                                <CardContent>
                                    <Grid container spacing={2}>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <TextField
                                                id="name-field"
                                                variant="standard"
                                                focused
                                                label="Nombre"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.name}
                                                onChange={handleChange('name')}
                                                style={{ width: 230 }}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <SegmentIcon color='primary' />
                                                        </InputAdornment>
                                                    )
                                                }}
                                                error={values.nameError}
                                                helperText={values.nameError && "Campo obligatorio"}
                                            />
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <TextField
                                                id="lastName-field"
                                                variant="standard"
                                                focused
                                                label="Apellidos"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.lastName}
                                                onChange={handleChange('lastName')}
                                                style={{ width: 230 }}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <SegmentIcon color='primary' />
                                                        </InputAdornment>
                                                    )
                                                }}
                                                error={values.lastNameError}
                                                helperText={values.lastNameError && "Campo obligatorio"}
                                            />
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <InputMask
                                                mask="999-999999-9999a"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.idCard}
                                                onChange={handleChange('idCard')}
                                            >
                                                {() => <TextField
                                                    id="idCard-field"
                                                    variant="standard"
                                                    focused
                                                    label="Cédula"
                                                    style={{ width: 230 }}
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position="start">
                                                                <BrandingWatermarkOutlinedIcon color='primary' />
                                                            </InputAdornment>
                                                        )
                                                    }}
                                                    error={values.idCardError}
                                                    helperText={values.idCardError && "Campo obligatorio"}
                                                />}
                                            </InputMask>
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <TextField
                                                id="address-field"
                                                variant="standard"
                                                focused
                                                multiline
                                                label="Dirección"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.address}
                                                onChange={handleChange('address')}
                                                style={{ width: 230 }}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <PlaceOutlinedIcon color='primary' />
                                                        </InputAdornment>
                                                    )
                                                }}
                                                error={values.addressError}
                                                helperText={values.addressError && "Campo obligatorio"}
                                            />
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <InputMask
                                                mask="9999-9999"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.phone}
                                                onChange={handleChange('phone')}
                                            >
                                                {() => <TextField
                                                    id="phone-field"
                                                    variant="standard"
                                                    focused
                                                    label="Teléfono"
                                                    style={{ width: 230 }}
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position="start">
                                                                <LocalPhoneOutlinedIcon color='primary' />
                                                            </InputAdornment>
                                                        )
                                                    }}
                                                    error={values.phoneError}
                                                    helperText={values.phoneError && "Campo obligatorio"}
                                                />}
                                            </InputMask>
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <TextField
                                                id="location-field"
                                                variant="standard"
                                                focused
                                                select
                                                label="Localidad"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.location}
                                                onChange={handleChange('location')}
                                                style={{ width: 230 }}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <MapOutlinedIcon color='primary' />
                                                        </InputAdornment>
                                                    )
                                                }}
                                                error={values.locationError}
                                                helperText={values.locationError && "Campo obligatorio"}
                                            >
                                                <MenuItem key="1" value="1">
                                                    Acoyapa
                                                </MenuItem>
                                                <MenuItem key="2" value="2">
                                                    Juigalpa
                                                </MenuItem>
                                            </TextField>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Card>
                                <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)', fontWeight: 'bold' } }} title="Datos servicio" />
                                <CardContent>
                                    <Grid container spacing={2}>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <TextField
                                                id="connect-field"
                                                variant="standard"
                                                focused
                                                label="N° Conexiones"
                                                disabled={dialogType === 2 ? true : false}
                                                value={values.connect}
                                                onChange={handleChange('connect')}
                                                type="number"
                                                style={{ width: 230 }}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <ConnectedTvOutlinedIcon color='primary' />
                                                        </InputAdornment>
                                                    )
                                                }}
                                                error={values.connectError}
                                                helperText={values.connectError && "Campo obligatorio"}
                                            />
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <LocalizationProvider locale={esLocale} dateAdapter={AdapterDateFns}>
                                                <DesktopDatePicker
                                                    label="Apertura"
                                                    inputFormat="dd/MM/yyyy"
                                                    value={values.created}
                                                    onChange={(newValue) => handleDateChange(newValue)}
                                                    renderInput={(params) =>
                                                        <TextField
                                                            focused
                                                            variant="standard"
                                                            error={values.createdError}
                                                            helperText={values.createdError && "Campo obligatorio"}
                                                            {...params}
                                                        />
                                                    }
                                                />
                                            </LocalizationProvider>
                                        </Grid>
                                        <Grid marginBottom={4} item xs={12} sm={12} md={4} lg={4}>
                                            <FormControl component="fieldset">
                                                <FormLabel style={{ fontSize: 13, color: '#1976d2' }} component="legend">Activo</FormLabel>
                                                <RadioGroup
                                                    row
                                                    aria-label="state"
                                                    name="row-radio-buttons-group"
                                                    value={values.isactive}
                                                    onChange={handleChange('isactive')}
                                                >
                                                    <FormControlLabel disabled={dialogType === 2 ? true : false} value="1" control={<Radio />} label="Si" />
                                                    <FormControlLabel disabled={dialogType === 2 ? true : false} value="2" control={<Radio />} label="No" />
                                                </RadioGroup>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions style={{ display: 'flex', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
                    <IconButton
                        onClick={() => handleClickOpen(false)}
                        title="Cancelar"
                        aria-label="Cancelar"
                        size="small"
                        style={{ marginRight: 10 }}
                    >
                        <CloseIcon color='primary' fontSize="medium" />
                    </IconButton>
                    <IconButton
                        onClick={saveHandler}
                        title="Guardar"
                        aria-label="Guardar"
                        size="small"
                    >
                        <SaveIcon color='primary' fontSize="medium" />
                    </IconButton>
                </DialogActions>
            </Dialog>
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
        </>
    );
}

export default AddAgreementView;
