import React from 'react';
import { Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import Transition from '../transition/transition';
import WarningIcon from '@mui/icons-material/Warning';

const WarningDialog = (props) => {
    const { open, handleClickOpen, handleAction, question, body } = props;
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <Dialog
            fullScreen={fullScreen}
            maxWidth='lg'
            TransitionComponent={Transition}
            open={open}
            onClose={() => handleClickOpen(false)}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle sx={{ display: 'flex', alignItems: 'center' }} color='red' id="responsive-dialog-title">
                <WarningIcon sx={{ mr: 1 }} /> {question}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    <Typography sx={{ width: 500 }}>
                        {body}
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button style={{ color: 'red' }} autoFocus onClick={() => handleClickOpen(false)}>
                    No
                </Button>
                <Button variant='contained' style={{ backgroundColor: 'red', color: '#fff' }}
                    onClick={() => handleAction()} autoFocus>
                    Si
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default WarningDialog;
