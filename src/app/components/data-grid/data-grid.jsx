import React from 'react'
import DataGridView from './data-grid.view';

const DataGrid = (props) => {
    const {
        rows,
        searchText,
        requestSearch,
        pageSize,
        setPageSize,
        selectionModel,
        handleClickOpen,
        handleSelection,
        columns,
        isLoading,
        isCrud,
        exFunction,
        exData,
        ExComp
    } = props;

    return (
        <DataGridView
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleClickOpen={handleClickOpen}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            isCrud={isCrud}
            exFunction={exFunction}
            exData={exData}
            ExComp={ExComp}
        />
    )
}

export default DataGrid;
