import React from 'react';
import {
    DataGrid,
    esES,
    GridToolbarFilterButton,
    GridToolbarDensitySelector,
    GridOverlay
} from "@mui/x-data-grid";
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import EditIcon from '@mui/icons-material/Edit';
import TextField from '@mui/material/TextField';
import LinearProgress from '@mui/material/LinearProgress';
import DeleteIcon from '@mui/icons-material/Delete';
import { useStyles } from '../../styles/data-grid';
import ClearIcon from '@mui/icons-material/Clear';
import SearchIcon from '@mui/icons-material/Search';

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}


QuickSearchToolbar.propTypes = {
    clearSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

function QuickSearchToolbar(props) {
    const { exFunction, exData, ExComp, classes, isCrud, value, onChange,
        handleClickOpen, selectionModel, clearSearch } = props;
    return (
        <Box className={classes.toolbarContainer}>
            <div className={classes.tableOptionsContainer}>
                <div>
                    <GridToolbarFilterButton />
                    <GridToolbarDensitySelector />
                </div>
                <TextField
                    variant="standard"
                    value={value}
                    onChange={onChange}
                    placeholder="Buscar…"
                    InputProps={{
                        startAdornment: <SearchIcon color='primary' fontSize="small" />,
                        endAdornment: (
                            <IconButton
                                title="Clear"
                                aria-label="Clear"
                                size="small"
                                style={{ visibility: value ? 'visible' : 'hidden' }}
                                onClick={clearSearch}
                            >
                                <ClearIcon fontSize="small" />
                            </IconButton>
                        ),
                    }}
                    sx={{
                        width: {
                            xs: 1,
                            sm: 'auto',
                        },
                        m: (theme) => theme.spacing(1, 0.5, 1.5),
                        '& .MuiSvgIcon-root': {
                            mr: 0.5,
                        },
                        '& .MuiInput-underline:before': {
                            borderBottom: 1,
                            borderColor: 'divider',
                        },
                    }}
                />
            </div>
            <Box className={classes.crudOptionsContainer}>
                {
                    exFunction && <ExComp exData={exData} handleFunction={exFunction} ></ExComp>
                }
                {
                    isCrud &&
                    <>
                        <IconButton
                            sx={{ mr: 1 }}
                            onClick={() => handleClickOpen(true)}
                            disabled={selectionModel.length > 0 ? true : false}
                            title="Crear"
                            aria-label="Crear"
                            size="small"
                        >
                            <AddCircleIcon color='primary' fontSize="medium" />
                        </IconButton>
                        <IconButton
                            sx={{ mr: 1 }}
                            onClick={() => handleClickOpen(true, 1)}
                            disabled={selectionModel.length > 0 ? false : true}
                            title="Editar"
                            aria-label="Editar"
                            size="small"
                        >
                            <EditIcon color='primary' fontSize="medium" />
                        </IconButton>
                        <IconButton
                            onClick={() => handleClickOpen(true, 2)}
                            disabled={selectionModel.length > 0 ? false : true}
                            title="Eliminar"
                            aria-label="Eliminar"
                            size="small"
                        >
                            <DeleteIcon color='primary' fontSize="medium" />
                        </IconButton>
                    </>
                }
            </Box>
        </Box>
    );
}


const DataGridView = (props) => {
    const classes = useStyles();
    const {
        searchText,
        rows,
        pageSize,
        setPageSize,
        selectionModel,
        handleClickOpen,
        handleSelection,
        requestSearch,
        columns,
        isLoading,
        isCrud,
        exFunction,
        exData,
        ExComp
    } = props;

    return (
        <div className={classes.tableContainer}>
            <DataGrid
                components={{
                    Toolbar: QuickSearchToolbar,
                    LoadingOverlay: CustomLoadingOverlay
                }}
                componentsProps={{
                    toolbar: {
                        isCrud: isCrud,
                        value: searchText,
                        classes: classes,
                        handleClickOpen: handleClickOpen,
                        selectionModel: selectionModel,
                        exFunction: exFunction,
                        exData: exData,
                        ExComp: ExComp,
                        onChange: (event) => requestSearch(event.target.value),
                        clearSearch: () => requestSearch(''),
                    }
                }}
                localeText={esES.components.MuiDataGrid.defaultProps.localeText}
                rows={rows}
                loading={isLoading}
                columns={columns}
                pageSize={pageSize}
                selectionModel={selectionModel}
                onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                pagination
                rowsPerPageOptions={[10, 30, 50]}
                onRowClick={(x) => { handleSelection(x) }}
            />
        </div>
    );
}

export default DataGridView;
