import React from 'react';
import MuiChip from '@mui/material/Chip';

const Chip = (params) => {
    const { row } = params;
    return (
        <MuiChip
            label={row.state === 1 ? 'Reportada' : (row.state === 2 ? 'En proceso' : 'Reparada')}
            color={row.state === 1 ? 'warning' : (row.state === 2 ? 'primary' : 'success')}
            size='small'
        />
    );
}

export default Chip;
