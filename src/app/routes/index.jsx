import React from "react";
import {
    BrowserRouter,
    Switch,
    Route
} from "react-router-dom";
import { connect } from 'react-redux';
import { onAuthChanged, selectUser } from '../../lib/data/user';
import { setCurrentUser } from '../../store/actions/auth';
import Landing from '../pages/landing/landing.view';
import Home from '../pages/home/home';
import Clients from '../pages/clients/clients';
import Agreement from '../pages/agreement/agreement';
import Payments from '../pages/payments/payments';
import Breakdown from '../pages/breakdown/breakdown';
import Reports from '../pages/reports/reports';
import AboutUs from '../pages/about-us/about-us';
import Location from '../pages/location/location';
import Settings from '../pages/settings/settings';
import Support from '../pages/support/support';
import AccountState from "../pages/account-state/account-state";
import LogIn from '../pages/auth/log-in/log-in';
import Welcome from '../pages/welcome/welcome';
import Load from '../pages/load/load';


const Index = (props) => {
    const { setCurrentUser, currentUser } = props;
    const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        const handleUserDetail = async (user) => {
            try {
                const userDetail = await selectUser(user.uid);
                setCurrentUser(userDetail);
            }
            catch (error) {
                console.error(error);
            }
            setIsLoading(false);
        };

        const listenAuthentication = () => {
            onAuthChanged((user) => {
                if (user) handleUserDetail(user);
                else {
                    setCurrentUser({ band: 0 });
                    setIsLoading(false);
                }
            });
        };

        listenAuthentication();
    }, [setCurrentUser]);

    if (isLoading) return <Load />
    else return <BrowserRouter>
        {
            currentUser.band === 0
                ? (
                    <Switch>
                        <Route exact path='/' component={Landing} />
                        <Route exact path='/logIn' component={LogIn} />
                        <Route component={Landing} />
                    </Switch>
                )
                : (
                    currentUser.band === 1 ? (
                        <Switch>
                            <Route exact path='/' component={Home} />
                            <Route exact path='/agreement' component={Agreement} />
                            <Route exact path='/clients' component={Clients} />
                            <Route exact path='/payments' component={Payments} />
                            <Route exact path='/breakdown' component={Breakdown} />
                            <Route exact path='/reports' component={Reports} />
                            <Route exact path='/settings' component={Settings} />
                            <Route exact path='/aboutUs' component={AboutUs} />
                            <Route exact path='/location' component={Location} />
                            <Route component={Home} />
                        </Switch>
                    ) : (
                        <Switch>
                            <Route exact path='/' component={Welcome} />
                            <Route exact path='/aboutUs' component={AboutUs} />
                            <Route exact path='/location' component={Location} />
                            <Route exact path='/support' component={Support} />
                            <Route exact path='/accountState' component={AccountState} />
                            <Route component={Welcome} />
                        </Switch>
                    )
                )
        }
    </BrowserRouter>
}

const mapStateToProps = state => ({
    currentUser: state.auth.currentUser
});

const mapDispatchToProps = dispatch => ({
    setCurrentUser: user => dispatch(setCurrentUser(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
