import React from 'react';
import { Typography, IconButton, Box } from '@mui/material';
import DataGrid from "../../components/data-grid/data-grid";
import Layout from '../../layout/app/app';
import SyncIcon from '@mui/icons-material/Sync';
import AccountState from '../../components/dialogs/account-state/account-state';

const PaymentsView = (props) => {
    const {
        searchText, rows, pageSize, setPageSize,
        selectionModel, handleSelection,
        requestSearch, columns, isLoading,
        openDetail, handleClickOpen, detailRws,
        detailInfo, handleState, setDetailRws,
        selectedRow, syncHandler, loadData
    } = props;

    return (
        <Layout>
            <React.Fragment >
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', pb: 1 }}>
                    <Typography variant="h6" gutterBottom component="div">Clientes</Typography>
                    <IconButton
                        title="Actualizar cuentas"
                        aria-label="Actualizar cuentas"
                        onClick={syncHandler}
                        color="primary"
                    >
                        <SyncIcon />
                    </IconButton>
                </Box>
                <AccountState
                    open={openDetail}
                    handleClickOpen={handleClickOpen}
                    isLoading={false}
                    detailRws={detailRws}
                    setDetailRws={setDetailRws}
                    detailInfo={detailInfo}
                    handleState={handleState}
                    client={selectedRow}
                    loadData={loadData}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={false}
                    exFunction={false}
                />
            </React.Fragment>
        </Layout>
    );
}

export default PaymentsView;
