import React from 'react';
import { searchHandler } from '../../../utils/functions';
import PaymentsView from './payments.view';
import { getClientsWithPayments, getPaymentsByClient, syncPayments } from '../../../lib/data/payments';

let tableRows = [];

let detailTbRws = [];

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "number", headerName: "N° Contrato", width: 250 },
    { field: "client", headerName: "Cliente", width: 250 },
    { field: "connection", headerName: "Conexiones", width: 180 },
    { field: "pending", headerName: "Facturas pendientes", width: 180 },
    { field: "debt", headerName: "Deuda total", width: 180 }
];

const Payments = () => {
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [detailRws, setDetailRws] = React.useState(detailTbRws);
    const [detailInfo, setDetailInfo] = React.useState({ pending: 0, debt: 0 });
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);
    const [openDetail, setOpenDetail] = React.useState(false);

    const handleClickOpen = (value) => {
        setOpenDetail(value);
        if (!value) handleSelection(selectedRow);
    }

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({payments: []});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
            handleState(x.row.number).then(() => {
                handleClickOpen(true);
            });
        }
    }

    const handleState = async (id) => {
        const detail = await getPaymentsByClient(id);
        setDetailRws(detail.payments);
        setDetailInfo({ pending: detail.pending, debt: detail.debt });
    }

    const syncHandler = () => {
        setisLoading(true);
        syncPayments().then(() => {
            loadData();
        });
    }

    const loadData = async () => {
        tableRows = await getClientsWithPayments();
        setRows(tableRows);
        setisLoading(false);
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <PaymentsView
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            openDetail={openDetail}
            handleClickOpen={handleClickOpen}
            detailRws={detailRws}
            detailInfo={detailInfo}
            handleState={handleState}
            setDetailRws={setDetailRws}
            selectedRow={selectedRow}
            syncHandler={syncHandler}
            loadData={loadData}
        />
    )
}

export default Payments;
