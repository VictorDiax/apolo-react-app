import React from 'react';
import { searchHandler } from '../../../utils/functions';
import { getLog } from '../../../lib/data/paymentsLog';
import ReportsVw from './reports.view';

let tableRows = [];

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "client", headerName: "Cliente", width: 250 },
    { field: "payments", headerName: "Meses facturados", width: 180 },
    { field: "debt", headerName: "Saldo anterior", width: 180 },
    { field: "total", headerName: "Pago C$", width: 180 },
    { field: "currentDebt", headerName: "Saldo actual", width: 180 },
    { field: "payed", headerName: "Fecha", width: 180 },
];

const Reports = () => {
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);
    const [value, setValue] = React.useState([null, null]);

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({ payments: [] });
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
        }
    }

    const loadData = async () => {
        tableRows = await getLog();
        setRows(tableRows);
        setisLoading(false);
    }

    const filtering = () => {
        return (x) => {
            return (value[0] !== null && value[1] !== null)
                ? (x.formatDate >= value[0].getTime() && x.formatDate < value[1].getTime()
                    ? true
                    : false
                )
                : true;
        }
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <ReportsVw
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            selectedRow={selectedRow}
            loadData={loadData}
            value={value}
            setValue={setValue}
            filtering={filtering}
        />
    )
}

export default Reports;
