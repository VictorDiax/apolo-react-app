import React from 'react';
import { Typography, Box, TextField, Alert, IconButton } from '@mui/material';
import DateRangePicker from '@mui/lab/DateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import esLocale from 'date-fns/locale/es';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import LocalPrintshopIcon from '@mui/icons-material/LocalPrintshop';
import AttachMoneyOutlinedIcon from '@mui/icons-material/AttachMoneyOutlined';
import DataGrid from '../../components/data-grid/data-grid';
import Layout from '../../layout/app/app';
import { PDFDownloadLink } from '@react-pdf/renderer';
import Pdf from '../../pdf/payment';

const ExComp = (props) => {
    const { handleFunction, exData } = props;
    return (
        <IconButton
            onClick={() => handleFunction(true)}
            disabled={exData.documentId === undefined ? true : false}
            title="Imprimir"
            aria-label="Imprimir"
            size="small"
        >
            <PDFDownloadLink document={<Pdf item={exData} />} fileName={`factura_pago_${exData.documentId}.pdf`}>
                <LocalPrintshopIcon color='success' fontSize="medium" />
            </PDFDownloadLink>
        </IconButton>
    );
}

const ReportsView = (props) => {
    const {
        searchText, rows, pageSize, setPageSize,
        selectionModel, handleSelection, requestSearch,
        columns, isLoading, selectedRow, value,
        setValue, filtering
    } = props;
    return (
        <Layout>
            <React.Fragment >
                <Box display='flex' flexDirection='row' justifyContent='space-between' alignItems='center' mb={1}>
                    <Typography variant="h6" gutterBottom component="div">Pagos</Typography>
                    <LocalizationProvider locale={esLocale} dateAdapter={AdapterDateFns}>
                        <DateRangePicker
                            startText={null}
                            endText={null}
                            value={value}
                            onChange={(newValue) => {
                                setValue(newValue);
                            }}
                            renderInput={(startProps, endProps) => (
                                <React.Fragment>
                                    <Box sx={{ mx: 2, color: '#1976d2' }}> Desde </Box>
                                    <TextField variant="standard" focused sx={{ width: 160, mt: 1 }} {...startProps} />
                                    <Box sx={{ mx: 2, color: '#1976d2' }}> Hasta </Box>
                                    <TextField variant="standard" focused sx={{ width: 160, mt: 1, mr: 2 }} {...endProps} />
                                    <Alert iconMapping={{
                                        info: <AttachMoneyOutlinedIcon fontSize="inherit" />,
                                    }} sx={{ width: 250, justifyContent: 'center' }} severity="info">Cobrado: {rows.filter(filtering()).reduce((sum, current) => sum + current.total, 0)}</Alert>
                                </React.Fragment>
                            )}
                        />
                    </LocalizationProvider>
                </Box>
                <DataGrid
                    rows={rows.filter(filtering())}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={false}
                    exFunction={() => { }}
                    exData={selectedRow}
                    ExComp={ExComp}
                />
            </React.Fragment>
        </Layout>
    );
}

export default ReportsView;
