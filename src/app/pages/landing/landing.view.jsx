import * as React from 'react';
import {
    Box, Paper, Typography, Grid,
    Card, CardMedia, CardHeader
} from '@mui/material';
import Layout from '../../layout/default/default';
import Bg from '../../assets/images/bg.jpg';

const styles = {
    paperContainer: {
        backgroundImage: ` linear-gradient(rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0.53)), url(${Bg})`,
        height: '80vh',
        borderRadius: 0,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
};

const Landing = () => {

    return (
        <Layout>
            <Paper style={styles.paperContainer}>
                <Box padding={5} display='flex' flexDirection='column' alignItems='center' justifyContent='center'>
                    <Typography sx={{ fontWeight: 'bold' }} marginBottom={4} color='white' variant="h3" component="div">EMPRESA TELEVISIÓN POR CABLE</Typography>
                    <Typography color='white' variant="h4" component="div">TV Cable Acoyapa Apolo 11</Typography>
                </Box>
            </Paper>
            <Box bgcolor='#d5dbe4' display='flex' flexDirection='column' alignItems='center' padding={4}>
                <Typography color='primary' mb={3} variant="h5" component="div">Programación en CANAL 17</Typography>
                <Grid padding={4} container spacing={2}>
                    <Grid display='flex' justifyContent='center' item xs={12} sm={12} md={6} lg={6}>
                        <Card sx={{ maxWidth: 345 }}>
                            <CardHeader
                                titleTypographyProps={{ sx: { color: 'rgb(0, 114, 229)' } }}
                                title="Alegría"
                                subheader="Fiesta patronales de la ciudad de Acoyapa"
                            />
                            <CardMedia
                                component="img"
                                height="300"
                                image="https://scontent.fmga3-1.fna.fbcdn.net/v/t31.18172-8/26850290_1028019497338143_1727097771065277216_o.jpg?_nc_cat=110&ccb=1-5&_nc_sid=cdbe9c&_nc_ohc=3TV_bDTivGYAX8QLqsA&tn=9QY8W8ohH94E5eNn&_nc_ht=scontent.fmga3-1.fna&oh=0f0a012d6402c8e375733bff5f9098f5&oe=61CD9D44"
                                alt="Alegría"
                            />
                        </Card>
                    </Grid>
                    <Grid display='flex' justifyContent='center' item xs={12} sm={12} md={6} lg={6}>
                        <Card sx={{ maxWidth: 345 }}>
                            <CardHeader
                                titleTypographyProps={{ sx: { color: 'rgb(0, 114, 229)' } }}
                                title="Diversión"
                                subheader="Fiesta patrias de la ciudad de Acoyapa"
                            />
                            <CardMedia
                                component="img"
                                height="300"
                                image="https://scontent.fmga5-1.fna.fbcdn.net/v/t1.6435-9/40877338_1194427544030670_6784298283125702656_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=e3f864&_nc_ohc=CdSZ5jMYco0AX-hEtcC&tn=VUWoIQshNnkHWzX7&_nc_ht=scontent.fmga5-1.fna&oh=00_AT8OY3i6PcCwICq3eah-rDM3uc9OZ3PMBHeojYC73Aj5rA&oe=61EECC6F"
                                alt="Diversión"
                            />
                        </Card>
                    </Grid>
                </Grid>
            </Box>
        </Layout>
    );
};

export default Landing;
