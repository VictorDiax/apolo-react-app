import React from 'react';
import { Typography } from '@mui/material';
import { useStyles } from '../../styles/home';
import Layout from '../../layout/app/app';
const WelcomeVw = () => {
    const classes = useStyles();
    return (
        <Layout>
            <React.Fragment >
                <section className={classes.root}>
                    <Typography variant="h4" gutterBottom component="div">Bienvenido!</Typography>
                </section>
            </React.Fragment>
        </Layout>
    );
}

export default WelcomeVw;
