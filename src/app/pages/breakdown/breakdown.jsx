import React from 'react';
import Chip from '../../components/chip/chip';
import { getBreaksWithClients } from '../../../lib/data/breakdowns';
import BreakdownVw from './breakdown.view';
import { searchHandler } from '../../../utils/functions';

let tableRows = [];
const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "number", headerName: "N° Contrato", width: 250 },
    { field: "client", headerName: "Cliente", width: 250 },
    { field: "description", headerName: "Avería", width: 400 },
    { field: "createdAt", headerName: "Fecha", width: 150 },
    { field: "state", headerName: "Estado", width: 200, renderCell: Chip }
];

const initialVals = {
    state: 0,
    staetError: false,
    description: '',
    address: ''
};

const Breakdown = () => {
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);
    const [dialogType, setDialogType] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const [values, setValues] = React.useState(initialVals);

    const handleClickOpen = (value, x = {}, type = 0,) => {
        setOpen(value);
        setDialogType(type);
        initValues(x);
        if (!value) {
            handleSelection(selectedRow);
            setValues(initialVals);
        }
    }

    const initValues = (x) => {
        setValues({
            ...initialVals,
            state: x.id !== undefined ? x.state : 0,
            description: x.id !== undefined ? x.description : '',
            address: x.id !== undefined ? x.address : '',
        });
    }

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
            handleClickOpen(true, x.row, 6);
        }
    }

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const loadData = async () => {
        tableRows = await getBreaksWithClients();
        setRows(tableRows);
        setisLoading(false);
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <BreakdownVw
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            selectedRow={selectedRow}
            handleClickOpen={handleClickOpen}
            open={open}
            values={values}
            setValues={setValues}
            loadData={loadData}
            dialog={dialogType}
        />
    )
}


export default Breakdown;

