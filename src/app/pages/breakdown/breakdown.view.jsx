import React from 'react';
import { Typography } from '@mui/material';
import DataGrid from "../../components/data-grid/data-grid";
import AddBreakdown from '../../components/dialogs/add-breakdown/add-breakdown';
import Layout from '../../layout/app/app';


const BreakdownView = (props) => {
    const { searchText, rows, pageSize, setPageSize, selectionModel, handleSelection,
        requestSearch, columns, isLoading, selectedRow, handleClickOpen,
        open, values, setValues, loadData, dialog } = props;

    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Averías</Typography>
                <AddBreakdown
                    open={open}
                    handleClickOpen={handleClickOpen}
                    loadData={loadData}
                    dialogType={dialog}
                    values={values}
                    setValues={setValues}
                    handleSelection={handleSelection}
                    selectedRow={selectedRow}
                    currentUser={false}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleClickOpen={handleClickOpen}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={false}
                />
            </React.Fragment>
        </Layout>
    );
}

export default BreakdownView;
