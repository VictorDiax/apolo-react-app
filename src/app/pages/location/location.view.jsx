import React from 'react';
import { Typography } from '@mui/material';
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import iconretinaurl from 'leaflet/dist/images/marker-icon-2x.png';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import Layout from '../../layout/app/app';

const LocationView = (props) => {
    const { location } = props;
    let DefaultIcon = L.icon({
        iconRetinaUrl: iconretinaurl,
        iconUrl: icon,
        shadowUrl: iconShadow
    })

    L.Marker.prototype.options.icon = DefaultIcon;

    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Ubicación</Typography>
                <MapContainer style={{width: '100%', height: '80vh'}} center={location} zoom={13}>
                    <TileLayer
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={location} >
                        <Popup>
                            TV Cable Acoyapa Apolo 11
                        </Popup>
                    </Marker>
                </MapContainer>
            </React.Fragment>
        </Layout>
    );
}

export default LocationView;
