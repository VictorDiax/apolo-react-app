import React from 'react'
import LocationView from './location.view';

const Location = () => {
    const location = [11.972535, -85.1729578]

    return (
        <LocationView location={location} />
    )
}

export default Location;
