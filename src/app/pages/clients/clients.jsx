import React from 'react'
import ClientsView from './clients.view';
import { getClientsWithUsers } from '../../../lib/data/user';
import { searchHandler } from '../../../utils/functions';

let tableRows = [];

const initialVals = {
    email: '',
    emailError: 0,
    password: '',
    passwordError: 0,
    confirm: '',
    confirmError: 0
};

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "number", headerName: "N° Contrato", width: 250 },
    { field: "name", headerName: "Nombre", width: 250 },
    { field: "lastName", headerName: "Apellidos", width: 250 },
    { field: "email", headerName: "Correo electrónico", width: 300 },
    { field: "hasUser", headerName: "Usuario", width: 180, type: "boolean" }
];

const Clients = () => {
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);
    const [open, setOpen] = React.useState(false);
    const [values, setValues] = React.useState(initialVals);

    const handleClickOpen = (value) => {
        setOpen(value);
        setValues({ ...values, email: selectedRow.email })
        if (!value) handleSelection(selectedRow);
    }

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
        }
    }

    const loadData = async () => {
        tableRows = await getClientsWithUsers();
        setRows(tableRows);
        setisLoading(false);
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <ClientsView
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            open={open}
            handleClickOpen={handleClickOpen}
            selectedRow={selectedRow}
            values={values}
            initialVals={initialVals}
            setValues={setValues}
            loadData={loadData}
        />
    )
}

export default Clients;
