import React from 'react';
import { Typography, IconButton } from '@mui/material';
import Layout from '../../layout/app/app';
import DataGrid from '../../components/data-grid/data-grid';
import PersonAddAltRoundedIcon from '@mui/icons-material/PersonAddAltRounded';
import AddUser from '../../components/dialogs/add-user/add-user';

const ExComp = (props) => {
    const { handleFunction, exData } = props;
    return (
        <IconButton
            onClick={() => handleFunction(true)}
            disabled={exData === undefined ? true : false}
            title="Crear usuario"
            aria-label="Crear usuario"
            size="small"
        >
            <PersonAddAltRoundedIcon color='primary' fontSize="medium" />
        </IconButton>
    );
};

const ClientsView = (props) => {
    const {
        searchText, rows, pageSize, setPageSize,
        selectionModel, handleSelection,
        requestSearch, columns, isLoading,
        open, handleClickOpen, selectedRow,
        values, initialVals, setValues,
        loadData
    } = props;

    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Clientes</Typography>
                <AddUser
                    open={open}
                    handleClickOpen={handleClickOpen}
                    selectedRow={selectedRow}
                    values={values}
                    initialVals={initialVals}
                    setValues={setValues}
                    loadData={loadData}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={false}
                    exFunction={handleClickOpen}
                    exData={selectedRow.number}
                    ExComp={ExComp}
                />
            </React.Fragment>
        </Layout>
    );
}

export default ClientsView;
