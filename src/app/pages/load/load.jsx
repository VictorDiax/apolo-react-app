import React from 'react';
import { Grid, Typography } from '@mui/material';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';

const LoadVw = () => {

    return (
        <>
            <Grid style={{
                backgroundImage: 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(164,38,199,1) 0.1%, rgba(65,220,255,1) 90.1% )',
                minHeight: '100vh',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
            }} item xs={12} sm={12}>
                <ConnectedTvIcon sx={{ fontSize: 60, color: '#fff', mr: 2 }} />
                <Typography variant="h2" color='white' noWrap component="div">Apolo 11</Typography>
            </Grid>
        </>
    );
}

export default LoadVw;