import React from 'react';
import { Typography, Grid, Card, CardHeader, CardContent, Box } from '@mui/material';
import { useStyles } from '../../styles/home';
import Layout from '../../layout/app/app';
import Image from '../../assets/images/about.jpg'
const AboutUsView = () => {
    const classes = useStyles();
    return (
        <Layout>
            <React.Fragment >
                <section className={classes.root}>
                    <Typography variant="h4" gutterBottom component="div">Sobre nosotros</Typography>
                </section>
                <Grid container spacing={2}>
                    <Grid display='flex' justifyContent='center' item xs={12} sm={12} md={12} lg={12}>
                        <img width={500} src={Image} alt="" />
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Card sx={{ display: 'flex' }}>
                            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                                <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)', fontWeight: 'bold' } }} title="Misión" />
                                <CardContent sx={{ flex: '1 0 auto', height: 150 }}>
                                    <Typography variant="body1" color="text.primary" component="div">
                                        Entregar a nuestro clientes soluciones integrales, confiables e innovadores en telecomunicaciones,
                                        tecnología y entretenimiento, comprometidos con brindarles un servicio superior en cada experiencia.
                                    </Typography>
                                </CardContent>
                            </Box>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Card sx={{ display: 'flex' }}>
                            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                                <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)', fontWeight: 'bold' } }} title="Visión" />
                                <CardContent sx={{ flex: '1 0 auto', height: 150 }}>
                                    <Typography variant="body1" color="text.primary" component="div">
                                        Ser la compañia lider de servicios de televisión por cable,
                                        preferida por su confiabilidad e innovación.
                                    </Typography>
                                </CardContent>
                            </Box>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Card>
                            <CardHeader sx={{ pb: 0 }} titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)', fontWeight: 'bold' } }} title="Valores" />
                            <CardContent sx={{ flex: '1 0 auto', height: 150 }}>
                                <Typography variant="body1" color="text.primary" component="div">
                                    1. Integridad
                                </Typography>
                                <Typography variant="body1" color="text.primary" component="div">
                                    2. Compromiso
                                </Typography>
                                <Typography variant="body1" color="text.primary" component="div">
                                    3. Respeto
                                </Typography>
                                <Typography variant="body1" color="text.primary" component="div">
                                    4. Excelencia
                                </Typography>
                                <Typography variant="body1" color="text.primary" component="div">
                                    5. Trabajo en equipo
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </React.Fragment>
        </Layout>
    );
}

export default AboutUsView;
