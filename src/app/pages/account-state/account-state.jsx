import React from 'react'
import AccountStateVw from './account-state.view';
import { connect } from 'react-redux';
import { getPaymentsByClient } from '../../../lib/data/payments';
import { searchHandler } from '../../../utils/functions';

let tableRows = [];

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "total", headerName: "C$ Valor", width: 250 },
    { field: "month", headerName: "Mes", width: 250 },
    { field: "year", headerName: "Año", width: 150 },
    { field: "topayed", headerName: "Fecha pago", width: 200 },
    { field: "payedDate", headerName: "Fecha cancelación", width: 200 },
    { field: "ispayed", headerName: "Estado", width: 150, type: "boolean" }
];

const AccountState = (props) => {
    const { currentUser } = props;
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [detail, setDetail] = React.useState({ pending: 0, debt: 0 });
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({ payments: [] });
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
        }
    }


    React.useEffect(() => {
        const loadData = async () => {
            const data = await getPaymentsByClient(currentUser.agree.id);
            tableRows = data.payments;
            setRows(data.payments);
            setDetail({ pending: data.pending, debt: data.debt })
            setisLoading(false);
        }

        loadData();
    }, [currentUser]);

    return (
        <AccountStateVw
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            detail={detail}
            agree={currentUser.agree}
            selectedRow={selectedRow}
        />
    )
}

const mapStateToProps = state => ({
    currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, null)(AccountState);

