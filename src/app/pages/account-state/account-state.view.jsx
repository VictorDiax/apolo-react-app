import React from 'react';
import { Typography, Box } from '@mui/material';
import Layout from '../../layout/app/app';
import DataGrid from '../../components/data-grid/data-grid';

const AccountStateVw = (props) => {
    const { searchText, rows, pageSize, setPageSize, agree,
        handleSelection, requestSearch, columns, isLoading, detail } = props;
    return (
        <Layout>
            <React.Fragment >
                <Box display='flex' flexDirection='row' justifyContent='space-between' alignItems='center' >
                    <Typography variant="h6" gutterBottom component="div">Estado de cuenta</Typography>
                    <Box display='flex' flexDirection='row' justifyContent='space-between' alignItems='center'>
                        <Typography mr={6} variant="subtitle1" gutterBottom component="div">Cliente: {`${agree.name} ${agree.lastName}`}</Typography>
                        <Typography mr={6} color='red' variant="subtitle1" gutterBottom component="div">Meses pendientes: {detail.pending}</Typography>
                        <Typography color='red' variant="subtitle1" gutterBottom component="div">Total: {detail.debt}</Typography>
                    </Box>
                </Box>
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={false}
                    exFunction={false}
                />
            </React.Fragment>
        </Layout>
    );
}

export default AccountStateVw;
