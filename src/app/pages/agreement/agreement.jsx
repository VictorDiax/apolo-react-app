import React from 'react'
import { AgreementView } from './agreement.view';
import { getAgreements } from '../../../lib/data/agreement';
import { searchHandler } from '../../../utils/functions';

const initialValues = {
    id: 0,
    name: '',
    nameError: false,
    lastName: '',
    lastNameError: false,
    idCard: '',
    idCardError: false,
    address: '',
    addressError: false,
    phone: '',
    phoneError: false,
    location: '',
    locationError: false,
    connect: '',
    connectError: false,
    created: null,
    createdError: false,
    isactive: '1'
};

let tableRows = [];

const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "number", headerName: "Nº Contrato", width: 200 },
    { field: "idCard", headerName: "Cédula", width: 180 },
    { field: "name", headerName: "Nombre", width: 180 },
    { field: "lastName", headerName: "Apellidos", width: 180 },
    { field: "address", headerName: "Dirección", width: 180 },
    { field: "phone", headerName: "Teléfono", width: 180 },
    { field: "createdAt", headerName: "Apertura", width: 100 }
];

const Agreement = () => {
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [open, setOpen] = React.useState(false);
    const [isLoading, setisLoading] = React.useState(true);
    const [dialogType, setDialogType] = React.useState(0);
    const [values, setValues] = React.useState(initialValues);

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
        }
    }

    const handleClickOpen = (value, type = 0) => {
        setOpen(value);
        setDialogType(type);
        initValues();
    };

    const initValues = () => {
        setValues(initialValues);
        if (selectedRow.id !== undefined) setValues({
            ...initialValues,
            id: selectedRow.number,
            name: selectedRow.name,
            lastName: selectedRow.lastName,
            idCard: selectedRow.idCard,
            address: selectedRow.address,
            phone: selectedRow.phone,
            location: selectedRow.location,
            connect: selectedRow.connection,
            isactive: selectedRow.isactive,
            created: selectedRow.created.toDate()
        });
    }

    const loadData = async () => {
        tableRows = await getAgreements();
        setRows(tableRows);
        setisLoading(false);
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <AgreementView
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            selectedRow={selectedRow}
            open={open}
            handleClickOpen={handleClickOpen}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            loadData={loadData}
            dialogType={dialogType}
            values={values}
            setValues={setValues}
            isLoading={isLoading}
        />
    )
}

export default Agreement;