import React from 'react';
import { Typography, IconButton } from '@mui/material';
import DataGrid from "../../components/data-grid/data-grid";
import Layout from '../../layout/app/app';
import LocalPrintshopIcon from '@mui/icons-material/LocalPrintshop';
import { AddAgreement } from '../../components/dialogs/add-agreement/add-agreement';
import { PDFDownloadLink } from '@react-pdf/renderer';
import Pdf from '../../pdf/agreement';

const ExComp = (props) => {
    const { handleFunction, exData } = props;
    return (
        <IconButton
            onClick={() => handleFunction(true)}
            disabled={exData.number === undefined ? true : false}
            title="Imprimir"
            aria-label="Imprimir"
            size="small"
        >
            <PDFDownloadLink document={<Pdf item={exData} />} fileName={`contrato_${exData.number}.pdf`}>
                <LocalPrintshopIcon color='success' fontSize="medium" />
            </PDFDownloadLink>
        </IconButton>
    );
}

export const AgreementView = (props) => {
    const {
        searchText,
        rows,
        pageSize,
        setPageSize,
        selectionModel,
        selectedRow,
        open,
        handleClickOpen,
        handleSelection,
        requestSearch,
        columns,
        loadData,
        dialogType,
        values,
        setValues,
        isLoading
    } = props;

    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Contratos</Typography>
                <AddAgreement
                    open={open}
                    handleClickOpen={handleClickOpen}
                    loadData={loadData}
                    dialogType={dialogType}
                    values={values}
                    setValues={setValues}
                    handleSelection={handleSelection}
                    selectedRow={selectedRow}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleClickOpen={handleClickOpen}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={true}
                    exFunction={() => { }}
                    exData={selectedRow}
                    ExComp={ExComp}
                />
            </React.Fragment>
        </Layout>
    );
}

