import React from 'react'
import LogInVw from './log-in.view';
import { signIn } from '../../../../lib/data/user';

const initialValues = {
    email: '',
    emailError: false,
    password: '',
    passwordError: false,
    isLoading: false
};

const LogIn = () => {
    const [values, setValues] = React.useState(initialValues);
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };

    const validate = () => {
        const { email, password } = values;
        let emailError = false, passwordError = false;

        if (email === '' || !email.includes('@') || !email.includes('.')) emailError = true; else emailError = false;
        if (password === '') passwordError = true; else passwordError = false;

        setValues({ ...values, emailError, passwordError });

        if (!emailError && !passwordError) return false;
        else return true;
    }

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleSave = () => {
        const error = validate();
        const { email, password } = values;
        if (!error) {
            setValues({ ...values, isLoading: true });
            signIn(email, password).then(() => {
                setValues(initialValues);
            }).catch((e) => {
                console.log(e);
                handleClickSnack('error', 'Error al iniciar sesión.Las credenciales son incorrectas.');
                setValues({ ...values, isLoading: false });
            });
        }
    }

    return (
        <LogInVw
            email={values.email}
            emailError={values.emailError}
            password={values.password}
            passwordError={values.passwordError}
            isLoading={values.isLoading}
            handleChange={handleChange}
            handleSave={handleSave}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
        />
    )
}

export default LogIn;
