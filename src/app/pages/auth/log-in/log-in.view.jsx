import React from 'react';
import {
    Typography, Box, Link, Grid, CssBaseline,
    Paper, TextField, Button, InputAdornment
} from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import Layout from '../../../layout/default/default';
import { useStyles } from '../../../styles/log-in';
import Toast from '../../../components/toast/toast';


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="/">
                TV Apolo
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const LogInVw = (props) => {
    const classes = useStyles();
    const { email, emailError, password, passwordError, isLoading, handleChange, handleSave,
        severity, message, openSnack, handleCloseSnack } = props;
    return (
        <Layout>
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
            <Box>
                <Grid container component="main" className={classes.root}>
                    <CssBaseline />
                    <Grid item xs={false} sm={4} md={8} className={classes.image} />
                    <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
                        <div className={classes.paper}>
                            <img alt='' src={require('../../../assets/images/apolo11.png')} className={classes.avatar} />
                            <Typography component="h1" variant="h5">
                                Inicio de sesión
                            </Typography>
                            <form className={classes.form} noValidate>
                                <TextField
                                    variant="standard"
                                    focused
                                    margin="normal"
                                    required
                                    fullWidth
                                    sx={{ mb: 5 }}
                                    type='email'
                                    id="email"
                                    label="Correo"
                                    name="email"
                                    autoComplete="email"
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <AccountCircleOutlinedIcon color='primary' />
                                            </InputAdornment>
                                        )
                                    }}
                                    error={emailError}
                                    value={email}
                                    onChange={handleChange('email')}
                                />
                                <TextField
                                    variant="standard"
                                    focused
                                    margin="normal"
                                    required
                                    fullWidth
                                    sx={{ mb: 5 }}
                                    name="password"
                                    label="Contraseña"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <LockOutlinedIcon color='primary' />
                                            </InputAdornment>
                                        )
                                    }}
                                    error={passwordError}
                                    value={password}
                                    onChange={handleChange('password')}
                                />
                                <Button
                                    fullWidth
                                    sx={{ mb: 5 }}
                                    variant="contained"
                                    color="primary"
                                    disabled={isLoading}
                                    className={classes.submit}
                                    onClick={handleSave}
                                >
                                    {isLoading ? 'Entrando...' : 'Entrar'}
                                </Button>
                                <Box>
                                    <Copyright />
                                </Box>
                            </form>
                        </div>
                    </Grid>
                </Grid>
            </Box>
        </Layout>
    );
}

export default LogInVw;
