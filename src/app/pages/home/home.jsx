import React from 'react'
import HomeView from './home.view';
import { paymentDate } from '../../../lib/data/services';
import { getBreakStats } from '../../../lib/data/breakdowns';
import { getPaymentsStats } from '../../../lib/data/payments';

const Home = () => {
    const [date, setDate] = React.useState('');
    const [breaks, setBreaks] = React.useState(null);
    const [pays, setPays] = React.useState({ amount: 0, clients: [] });

    const loadData = async () => {
        const date = await paymentDate();
        const breaks = await getBreakStats();
        const data = await getPaymentsStats();
        setDate(date);
        setBreaks(breaks);
        setPays(data);
    }

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <HomeView
            date={date}
            breaks={breaks}
            pays={pays}
        />
    )
}

export default Home;
