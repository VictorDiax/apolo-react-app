import React from 'react';
import { Typography, Grid, Card, CardHeader, CardContent } from '@mui/material';
import Layout from '../../layout/app/app';
import { PieChart, ColumnChart } from 'react-chartkick';
import 'chartkick/chart.js'

const HomeView = (props) => {
    const { date, breaks, pays } = props;
    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Inicio</Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Grid container spacing={4}>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                <Card>
                                    <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)' } }} title="Monto a recuperar" />
                                    <CardContent>
                                        <Typography variant="h4" gutterBottom component="div">
                                            C$ {pays.amount}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                <Card>
                                    <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)' } }} title="Fecha de cobro" />
                                    <CardContent>
                                        <Typography variant="h4" gutterBottom component="div">
                                            {date}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Card>
                            <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)' } }} title="Averías" />
                            <CardContent>
                                <PieChart data={breaks} />
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <Card>
                            <CardHeader titleTypographyProps={{ sx: { fontSize: 16, color: 'rgb(0, 114, 229)' } }} title="Clientes" />
                            <CardContent>
                                <ColumnChart data={pays.clients} />
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </React.Fragment>
        </Layout>
    );
}

export default HomeView;
