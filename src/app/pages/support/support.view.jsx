import React from 'react';
import { Typography } from '@mui/material';
import Layout from '../../layout/app/app';
import DataGrid from "../../components/data-grid/data-grid";
import AddBreakdown from '../../components/dialogs/add-breakdown/add-breakdown';


const SupportView = (props) => {
    const { searchText, rows, pageSize, setPageSize, selectionModel, handleSelection,
        requestSearch, columns, isLoading, selectedRow, currentUser, handleClickOpen,
        open, values, setValues, loadData, dialog } = props;

    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Soporte</Typography>
                <AddBreakdown
                    open={open}
                    handleClickOpen={handleClickOpen}
                    loadData={loadData}
                    dialogType={dialog}
                    values={values}
                    setValues={setValues}
                    handleSelection={handleSelection}
                    selectedRow={selectedRow}
                    currentUser={currentUser.agree}
                />
                <DataGrid
                    rows={rows}
                    searchText={searchText}
                    requestSearch={requestSearch}
                    pageSize={pageSize}
                    setPageSize={setPageSize}
                    selectionModel={selectionModel}
                    handleClickOpen={handleClickOpen}
                    handleSelection={handleSelection}
                    columns={columns}
                    isLoading={isLoading}
                    isCrud={true}
                />
            </React.Fragment>
        </Layout>
    );
}

export default SupportView;
