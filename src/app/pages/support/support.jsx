import React from 'react';
import Chip from '../../components/chip/chip';
import { connect } from 'react-redux';
import { getBreaksByClient } from '../../../lib/data/breakdowns';
import SupportVw from './support.view';
import { searchHandler } from '../../../utils/functions';

let tableRows = [];
const columns = [
    { field: "id", headerName: "#", width: 100 },
    { field: "number", headerName: "N° Contrato", width: 250 },
    { field: "client", headerName: "Cliente", width: 250 },
    { field: "description", headerName: "Avería", width: 400 },
    { field: "createdAt", headerName: "Fecha", width: 150 },
    { field: "state", headerName: "Estado", width: 200, renderCell: Chip }
];

const initialVals = {
    description: '',
    descriptionError: false
};

const Support = (props) => {
    const { currentUser } = props;
    const [searchText, setSearchText] = React.useState('');
    const [rows, setRows] = React.useState(tableRows);
    const [pageSize, setPageSize] = React.useState(10);
    const [selectionModel, setSelectionModel] = React.useState([]);
    const [selectedRow, setSelectedRow] = React.useState({});
    const [isLoading, setisLoading] = React.useState(true);
    const [dialogType, setDialogType] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const [values, setValues] = React.useState(initialVals);

    const handleClickOpen = (value, type = 0) => {
        setOpen(value);
        setDialogType(type);
        initValues();
        if (!value) {
            handleSelection(selectedRow);
            setValues(initialVals);
        }
    }

    const initValues = () => {
        setValues({ ...initialVals, description: selectedRow.id !== undefined ? selectedRow.description : '' });
    }

    const handleSelection = (x) => {
        if (selectedRow.id === x.id) {
            setSelectionModel([]);
            setSelectedRow({});
        }
        else {
            setSelectionModel([x.id]);
            setSelectedRow(x.row);
        }
    }

    const requestSearch = (searchValue) => {
        searchHandler(searchValue, tableRows, setSearchText, setRows);
    };

    const loadData = async () => {
        tableRows = await getBreaksByClient(currentUser);
        setRows(tableRows);
        setisLoading(false);
    }

    React.useEffect(() => {
        loadData();
    });

    return (
        <SupportVw
            searchText={searchText}
            rows={rows}
            pageSize={pageSize}
            setPageSize={setPageSize}
            selectionModel={selectionModel}
            handleSelection={handleSelection}
            requestSearch={requestSearch}
            columns={columns}
            isLoading={isLoading}
            selectedRow={selectedRow}
            currentUser={currentUser}
            handleClickOpen={handleClickOpen}
            open={open}
            values={values}
            setValues={setValues}
            loadData={loadData}
            dialog={dialogType}
        />
    )
}

const mapStateToProps = state => ({
    currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, null)(Support);

