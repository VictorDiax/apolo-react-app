import React from 'react';
import {
    Typography, Card, Grid,
    CardActions, CardContent,
    Button, InputAdornment,
    TextField, MenuItem
} from '@mui/material';
import InputMask from 'react-input-mask';
import ConnectedTvOutlinedIcon from '@mui/icons-material/ConnectedTvOutlined';
import AttachMoneyOutlinedIcon from '@mui/icons-material/AttachMoneyOutlined';
import TodayOutlinedIcon from '@mui/icons-material/TodayOutlined';
import Layout from '../../layout/app/app';
import Toast from '../../components/toast/toast';

const SettingsView = (props) => {
    const {
        days, values, handleChange,
        saveHandler, severity,
        message, openSnack,
        handleCloseSnack
    } = props;
    return (
        <Layout>
            <React.Fragment >
                <Typography variant="h6" gutterBottom component="div">Configuraciones</Typography>
                <Card sx={{ minWidth: 275 }}>
                    <CardContent>
                        <Grid padding={4} container spacing={2}>
                            <Grid marginBottom={5} item xs={12} sm={12} md={4} lg={4}>
                                <TextField
                                    id="location-field"
                                    variant="standard"
                                    focused
                                    select
                                    label="Servicio"
                                    disabled
                                    value={values.description}
                                    onChange={handleChange('description')}
                                    style={{ width: 230 }}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <ConnectedTvOutlinedIcon color='primary' />
                                            </InputAdornment>
                                        )
                                    }}
                                    error={values.descriptionError}
                                    helperText={values.descriptionError && "Campo obligatorio"}
                                >
                                    <MenuItem key={values.description} value={values.description}>
                                        {values.description}
                                    </MenuItem>
                                </TextField>
                            </Grid>
                            <Grid marginBottom={5} item xs={12} sm={12} md={4} lg={4}>
                                <TextField
                                    id="location-field"
                                    variant="standard"
                                    focused
                                    select
                                    label="Día de cobro"
                                    value={values.day}
                                    onChange={handleChange('day')}
                                    style={{ width: 230 }}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <TodayOutlinedIcon color='primary' />
                                            </InputAdornment>
                                        )
                                    }}
                                    error={values.dayError}
                                    helperText={values.dayError && "Campo obligatorio"}
                                >
                                    {days.map((item) => (
                                        <MenuItem
                                            key={item}
                                            value={item}
                                        >
                                            {item}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>
                            <Grid marginBottom={5} item xs={12} sm={12} md={4} lg={4}>
                                <InputMask
                                    mask="999.99"
                                    value={values.price}
                                    onChange={handleChange('price')}
                                >
                                    {() => <TextField
                                        id="location-field"
                                        variant="standard"
                                        focused
                                        label="Precio"
                                        style={{ width: 230 }}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <AttachMoneyOutlinedIcon color='primary' />
                                                </InputAdornment>
                                            )
                                        }}
                                        error={values.priceError}
                                        helperText={values.priceError && "Campo obligatorio"}
                                    />}
                                </InputMask>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Button onClick={saveHandler} style={{ margin: 20 }} color='primary' variant='contained' size="small">
                            Actualizar
                        </Button>
                    </CardActions>
                </Card>
            </React.Fragment>
            <Toast severity={severity} message={message} open={openSnack} handleClose={handleCloseSnack} />
        </Layout>
    );
}

export default SettingsView;
