import React from 'react'
import SettingsView from './settings.view';
import { getServices, updateService } from '../../../lib/data/services';

const initialValues = {
    id: 0,
    description: '',
    descriptionError: false,
    price: '',
    priceError: false,
    day: '',
    dayError: false
};

const Settings = () => {
    const [days] = React.useState([
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25
    ]);
    const [values, setValues] = React.useState(initialValues);
    const [openSnack, setOpenSnack] = React.useState(false);
    const [severity, setSeverity] = React.useState('info');
    const [message, setMessage] = React.useState('');

    const handleClickSnack = (valSev, valMsg) => {
        setOpenSnack(true);
        setSeverity(valSev);
        setMessage(valMsg);
    };

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnack(false);
        setSeverity('');
        setMessage('');
    };

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const loadData = async () => {
        let services = await getServices();
        setValues({ ...initialValues, ...services[0] });
    }

    const validate = () => {
        const { description, day, price } = values;
        let descriptionError = false;
        let dayError = false;
        let priceError = false;

        if (description === '') descriptionError = true; else descriptionError = false;

        if (day === '') dayError = true; else dayError = false;

        if (price === '') priceError = true; else priceError = false;

        setValues({
            ...values,
            descriptionError,
            dayError,
            priceError
        });

        if (!descriptionError && !dayError && !priceError) return false;
        else return true;
    }

    const saveHandler = () => {
        const error = validate();
        if (!error) {
            updateService({ price: values.price, day: values.day }, values.id).then(() => {
                loadData();
                handleClickSnack('success', 'Las configuraciones han sido actualizadas de forma exitosa!');
            });
        }
    }


    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <SettingsView
            days={days}
            values={values}
            handleChange={handleChange}
            saveHandler={saveHandler}
            severity={severity}
            message={message}
            openSnack={openSnack}
            handleCloseSnack={handleCloseSnack}
        />
    )
}

export default Settings;
