import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    root: {
        color: '#000',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
});