import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
    tableContainer: {
        height: 600,
        width: "100%"
    },
    toolbarContainer: {
        padding: 10,
        justifyContent: 'space-between',
        display: 'flex',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
    },
    tableOptionsContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 500
    },
    crudOptionsContainer: {
        display: 'flex',
        alignItems: 'center',
        width: 120,
        justifyContent: 'end'
    }
});
