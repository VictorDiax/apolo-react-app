import { makeStyles } from '@mui/styles';
import Bg from '../assets/images/city.jpg';

const useStyles = makeStyles({
    root: {
        height: '90.95vh',
    },
    image: {
        backgroundImage: ` linear-gradient(rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0.53)), url(${Bg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: 40,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: 20,
        width: 150,
        height: 150
    },
    form: {
        width: '100%',
        marginTop: 10,
    },
    submit: {
        margin: 10
    },
});

export { useStyles };
