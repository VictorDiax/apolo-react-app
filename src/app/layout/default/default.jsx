import React from 'react';
import DefaultVw from './default.view';
import { withRouter } from 'react-router-dom';

const Default = (props) => {
    const { children, history } = props;
    const [anchorElNav, setAnchorElNav] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const goPage = (route) => {
        history.push(route);
    }

    return (
        <DefaultVw
            children={children}
            handleOpenNavMenu={handleOpenNavMenu}
            handleCloseNavMenu={handleCloseNavMenu}
            anchorElNav={anchorElNav}
            goPage={goPage}
        />
    )
}

export default withRouter(Default);