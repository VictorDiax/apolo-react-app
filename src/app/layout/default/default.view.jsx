import * as React from 'react';
import {
    AppBar, Box, Toolbar,
    IconButton, Typography, Menu,
    Container, Button, MenuItem
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';

const pages = [
    { title: 'Servicios', route: '/' },
    { title: 'Noticias', route: '/' }
];
const logo = 'Apolo 11';

const DefaultVw = (props) => {
    const { children, handleOpenNavMenu, handleCloseNavMenu, anchorElNav, goPage } = props;


    return (
        <>
            <AppBar position="static">
                <Container style={{ background: 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(164,38,199,1) 0.1%, rgba(65,220,255,1) 90.1% )' }} maxWidth="xl">
                    <Toolbar disableGutters>
                        <Box onClick={() => goPage('/')} sx={{ mr: 2, display: { xs: 'none', md: 'flex' }, alignItems: 'center' }}>
                            <ConnectedTvIcon sx={{ fontSize: 25, color: '#fff', mr: 1 }} />
                            <Typography
                                variant="h6"
                                noWrap
                                component="div"
                            >
                                {logo}
                            </Typography>
                        </Box>
                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="inherit"
                            >
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: { xs: 'block', md: 'none' },
                                }}
                            >
                                {pages.map((page) => (
                                    <MenuItem key={page.title} onClick={() => { handleCloseNavMenu(); goPage(page.route) }}>
                                        <Typography textAlign="center">{page.title}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                        <Box onClick={() => goPage('/')} sx={{ fontWeight: 'bold', flexGrow: 1, display: { xs: 'flex', md: 'none' }, alignItems: 'center' }}>
                            <ConnectedTvIcon sx={{ fontSize: 25, color: '#fff', mr: 1 }} />
                            <Typography
                                variant="h6"
                                noWrap
                                component="div"
                            >
                                {logo}
                            </Typography>
                        </Box>
                        <Box sx={{ justifyContent: 'end', flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                            {pages.map((page) => (
                                <Button
                                    key={page.title}
                                    onClick={() => { handleCloseNavMenu(); goPage(page.route) }}
                                    sx={{ my: 2, color: 'white', display: 'block', textTransform: 'none' }}
                                >
                                    {page.title}
                                </Button>
                            ))}
                        </Box>

                        <Box sx={{ flexGrow: 0 }}>
                            <Button onClick={() => goPage('/logIn')} style={{ ml: 4, height: 35, backgroundColor: '#fff', color: '#45d5fd' }} >
                                Iniciar sesión
                            </Button>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
            <Box sx={{ height: '50vh' }}>
                {children}
            </Box>
        </>
    );
};

export default DefaultVw;
