import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import {
    Box, Drawer as MuiDrawer, AppBar as MuiAppBar, Toolbar, List, CssBaseline,
    Typography, Divider, IconButton, ListItem, ListItemIcon,
    ListItemText, Tooltip, Menu, MenuItem
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import HomeIcon from '@mui/icons-material/Home';
import ArticleIcon from '@mui/icons-material/Article';
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import FolderIcon from '@mui/icons-material/Folder';
import AccountCircle from '@mui/icons-material/AccountCircle';
import ReportProblemIcon from '@mui/icons-material/ReportProblem';
import PeopleIcon from '@mui/icons-material/People';
import GroupsIcon from '@mui/icons-material/Groups';
import PlaceIcon from '@mui/icons-material/Place';
import SettingsIcon from '@mui/icons-material/Settings';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';
import HomeRepairServiceIcon from '@mui/icons-material/HomeRepairService';
import Warning from '../../components/dialogs/warning/warning';

const drawerWidth = 240;

const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

const AppVw = (props) => {
    const { open, handleDrawerOpen, goPage, children, pathname,
        handleMenu, handleClose, anchorEl, handleOpenWarn, warnVals, signOut, user } = props;
    const theme = useTheme();

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed" open={open}>
                <Toolbar style={{ display: 'flex', justifyContent: 'space-between', background: 'radial-gradient( circle farthest-corner at 10% 20%,  rgba(164,38,199,1) 0.1%, rgba(65,220,255,1) 90.1% )' }}>
                    <Box display='flex'>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => handleDrawerOpen(true)}
                            edge="start"
                            sx={{
                                marginRight: '25px',
                                ...(open && { display: 'none' }),
                            }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Box display='flex' alignItems='center'>
                            <ConnectedTvIcon sx={{ fontSize: 25, color: '#fff', mr: 1 }} />
                            <Typography variant="h6" noWrap component="div">
                                Apolo 11
                            </Typography>
                        </Box>
                    </Box>
                    <Box >
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircle sx={{ fontSize: 30 }} />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            sx={{ mt: '45px' }}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleClose}>Perfil</MenuItem>
                            <MenuItem
                                onClick={() => {
                                    handleOpenWarn(true, '¿Seguro que desea cerrar sesión?', 'Está apunto de salir del sistema!');
                                    handleClose()
                                }}
                            >
                                Cerrar sesión
                            </MenuItem>
                        </Menu>
                    </Box>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <DrawerHeader>
                    <IconButton onClick={() => handleDrawerOpen(false)}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    <ListItem button onClick={() => goPage("/")} selected={pathname === '/' ? true : false} >
                        <Tooltip title={open ? "" : "Inicio"} placement="right-start">
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary='Inicio' />
                    </ListItem>
                    {
                        user.band === 1 ?
                            <>
                                <ListItem button onClick={() => goPage("/agreement")} selected={pathname === '/agreement' ? true : false}>
                                    <Tooltip title={open ? "" : "Contratos"} placement="right-start">
                                        <ListItemIcon>
                                            <FolderIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Contratos' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/payments")} selected={pathname === '/payments' ? true : false}>
                                    <Tooltip title={open ? "" : "Caja"} placement="right-start">
                                        <ListItemIcon>
                                            <PointOfSaleIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Caja' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/reports")} selected={pathname === '/reports' ? true : false}>
                                    <Tooltip title={open ? "" : "Pagos"} placement="right-start">
                                        <ListItemIcon>
                                            <ArticleIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Pagos' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/breakdown")} selected={pathname === '/breakdown' ? true : false}>
                                    <Tooltip title={open ? "" : "Averías"} placement="right-start">
                                        <ListItemIcon>
                                            <ReportProblemIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Averías' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/clients")} selected={pathname === '/clients' ? true : false} >
                                    <Tooltip title={open ? "" : "Clientes"} placement="right-start">
                                        <ListItemIcon>
                                            <GroupsIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Clientes' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/settings")} selected={pathname === '/settings' ? true : false}>
                                    <Tooltip title={open ? "" : "Configuraciones"} placement="right-start">
                                        <ListItemIcon>
                                            <SettingsIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Configuraciones' />
                                </ListItem>
                            </> :
                            <>
                                <ListItem button onClick={() => goPage("/accountState")} selected={pathname === '/accountState' ? true : false}>
                                    <Tooltip title={open ? "" : "Estado de cuenta"} placement="right-start">
                                        <ListItemIcon>
                                            <ArticleIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Estado de cuenta' />
                                </ListItem>
                                <ListItem button onClick={() => goPage("/support")} selected={pathname === '/support' ? true : false}>
                                    <Tooltip title={open ? "" : "Soporte"} placement="right-start">
                                        <ListItemIcon>
                                            <HomeRepairServiceIcon />
                                        </ListItemIcon>
                                    </Tooltip>
                                    <ListItemText primary='Soporte' />
                                </ListItem>
                            </>
                    }
                </List>
                <Divider />
                <List>
                    <ListItem button onClick={() => goPage("/aboutUs")} selected={pathname === '/aboutUs' ? true : false}>
                        <Tooltip title={open ? "" : "Nosotros"} placement="right-start">
                            <ListItemIcon>
                                <PeopleIcon />
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary='Nosotros' />
                    </ListItem>
                    <ListItem button onClick={() => goPage("/location")} selected={pathname === '/location' ? true : false}>
                        <Tooltip title={open ? "" : "Ubicación"} placement="right-start">
                            <ListItemIcon>
                                <PlaceIcon />
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary='Ubicación' />
                    </ListItem>
                </List>
            </Drawer>
            <Box component="main" sx={{ flexGrow: 1, p: 3, height: '100vh' }}>
                <DrawerHeader />
                <Warning
                    handleClickOpen={handleOpenWarn}
                    open={warnVals.isOpen}
                    handleAction={signOut}
                    question={warnVals.question}
                    body={warnVals.body}
                />
                {children}
            </Box>
        </Box>
    );
}

export default AppVw
