import React from 'react';
import AppVw from './app.view';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { handleSignOut } from '../../../lib/data/user';

const warnInitial = {
    isOpen: false,
    question: '',
    body: ''
};

const App = (props) => {
    const { children, history, location, currentUser } = props;
    const [open, setOpen] = React.useState(false);
    const [warnVals, setWarnVals] = React.useState(warnInitial);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleOpenWarn = (isOpen, question, body) => {
        setWarnVals({ isOpen, question, body })
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDrawerOpen = (value) => {
        setOpen(value);
    };

    const goPage = (route) => {
        history.push(route);
    }

    const signOut = () => {
        handleSignOut();
        history.push("/");
    }

    return (
        <AppVw
            open={open}
            handleDrawerOpen={handleDrawerOpen}
            goPage={goPage}
            children={children}
            pathname={location.pathname}
            handleMenu={handleMenu}
            handleClose={handleClose}
            anchorEl={anchorEl}
            handleOpenWarn={handleOpenWarn}
            warnVals={warnVals}
            signOut={signOut}
            user={currentUser}
        />
    )
}

const mapStateToProps = state => ({
    currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, null)(withRouter(App));
