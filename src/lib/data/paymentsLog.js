import { db } from '../firebase';
import { format } from 'date-fns';
import { collection, query, addDoc, deleteDoc, doc, getDocs, orderBy, updateDoc } from 'firebase/firestore';

const PAYS_LOG_KEY = 'PaymentsLog';


export const getLog = async () => {
    let list = [];
    const q = query(collection(db, PAYS_LOG_KEY), orderBy("date", "desc"));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            payed: format(doc.data().date.toDate(), 'dd/MM/yyyy'),
            payments: doc.data().months.length,
            documentId: doc.id,
            formatDate: doc.data().date.toDate().getTime(),
            ...doc.data()
        });
    });
    return list;
}

export const createPaymentLog = async (payment) => {
    await addDoc(collection(db, PAYS_LOG_KEY), { ...payment });
}

export const updatePaymentLog = async (payment, id) => {
    await updateDoc(doc(db, PAYS_LOG_KEY, id), { ...payment });
}

export const deletePaymentLog = async (id) => {
    await deleteDoc(doc(db, PAYS_LOG_KEY, id));
}
