import { db } from '../firebase';
import { getAgreements } from './agreement';
import { getServices } from './services';
import { createPaymentLog } from './paymentsLog';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import { collection, query, addDoc, deleteDoc, doc, getDocs, orderBy, updateDoc, where } from 'firebase/firestore';

const PAYS_KEY = 'Payments';

const addDays = (value, day, month) => {
    var date = new Date(value);
    date.setDate(day);
    date.setMonth(date.getMonth() + month);
    return date;
}

const setPayments = (day, date, id, connection, unitPrice) => {
    var stop = new Date();
    var start = addDays(date, day, 1);
    var current = start;
    while (current < stop) {
        createPayment({
            agreement: id, date: new Date(current), ispayed: false,
            connection, unitPrice, total: unitPrice * connection
        });
        current = addDays(current, day, 1);
    }
}

export const syncPayments = async () => {
    const service = await getServices();
    const clients = await getClientsWithPayments();
    clients.forEach((item) => {
        let date = item.payments.length > 0 ? item.payments[0].date.toDate() : item.created.toDate();
        setPayments(service[0].day, date, item.number, parseInt(item.connection), service[0].price);
    });
};


export const getPayments = async () => {
    let list = [];
    const q = query(collection(db, PAYS_KEY), orderBy("date", "desc"));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            month: format(doc.data().date.toDate(), 'LLLL', { locale: es }),
            year: format(doc.data().date.toDate(), 'yyyy'),
            topayed: format(doc.data().date.toDate(), 'dd/MM/yyyy'),
            payed: doc.data().payed ? format(doc.data().payed.toDate(), 'dd/MM/yyyy') : '',
            documentId: doc.id,
            ...doc.data()
        });
    });
    return list;
}

export const checkPaymentsByClient = async (client) => {
    let list = [];
    const q = query(collection(db, PAYS_KEY), where("agreement", "==", client));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc) => {
        list.push({ id: doc.id, ...doc.data() });
    });
    return list.length > 0 ? true : false;
}

export const getPaymentsStats = async () => {
    const pays = await getPayments();
    const pending = pays.filter(x => !x.ispayed);
    const payed = pays.filter(x => x.ispayed)
    const uniquePg = pending.filter((v, i, a) => a.findIndex(t => (t.agreement === v.agreement)) === i);
    const uniquePd = payed.filter((v, i, a) => a.findIndex(t => (t.agreement === v.agreement)) === i);

    const pdClients = uniquePd.filter((el) => {
        return uniquePg.every((f) => { return f.agreement !== el.agreement; });
    });

    return {
        amount: pending.reduce((sum, current) => sum + current.total, 0),
        clients: [["Pendientes", uniquePg.length], ["Al corriente", pdClients.length]]
    };
}

export const getPaymentsByClient = async (client) => {
    let list = [];
    const q = query(collection(db, PAYS_KEY), where("agreement", "==", client));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            month: format(doc.data().date.toDate(), 'LLLL', { locale: es }),
            year: format(doc.data().date.toDate(), 'yyyy'),
            topayed: format(doc.data().date.toDate(), 'dd/MM/yyyy'),
            payedDate: doc.data().payed ? format(doc.data().payed.toDate(), 'dd/MM/yyyy') : '',
            documentId: doc.id,
            ...doc.data()
        });
    });
    const sortList = list.sort((a, b) => {
        var dateA = a.date.toDate();
        var dateB = b.date.toDate();
        return dateA < dateB ? 1 : -1;
    });
    let notPayed = sortList.filter(x => !x.ispayed);
    return { payments: sortList, pending: notPayed.length, debt: notPayed.reduce((sum, current) => sum + current.total, 0) };
}

export const getClientsWithPayments = async () => {
    const clients = await getAgreements();
    const payments = await getPayments();
    let list = [];
    clients.forEach((item) => {
        let newItem = item;
        newItem.client = `${item.name} ${item.lastName}`;
        newItem.payments = payments.filter(x => x.agreement === item.number);
        let pendings = newItem.payments.filter(x => !x.ispayed);
        newItem.pending = pendings.length;
        newItem.debt = pendings.reduce((sum, current) => sum + current.total, 0);
        list.push(newItem);
    });
    return list;
}


export const createPayment = async (payment) => {
    await addDoc(collection(db, PAYS_KEY), { ...payment });
}

export const loadPayments = async (toPay, data, debt) => {
    const date = new Date();
    const total = toPay.reduce((sum, current) => sum + current.total, 0);
    createPaymentLog({
        months: toPay,
        client: `${data.name} ${data.lastName}`,
        agreement: data.number,
        date: date, total: total,
        debt, currentDebt: debt - total
    });
    toPay.forEach((item) => {
        updatePayment({ ispayed: true, payed: new Date() }, item.documentId)
    });
}

export const updatePayment = async (payment, id) => {
    await updateDoc(doc(db, PAYS_KEY, id), { ...payment });
}

export const deletePayment = async (id) => {
    await deleteDoc(doc(db, PAYS_KEY, id));
}
