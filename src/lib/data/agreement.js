import { db } from '../firebase';
import { format } from 'date-fns';
import { getServices } from './services';
import { collection, addDoc, deleteDoc, doc, query, where, getDocs, updateDoc } from 'firebase/firestore';

const AGREES_KEY = 'Agreements';

export const getAgreements = async () => {
    const service = await getServices();
    let list = [];
    const querySnapshot = await getDocs(collection(db, AGREES_KEY));
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            number: doc.id,
            createdAt: format(doc.data().created.toDate(), 'dd/MM/yyyy'),
            service: service[0].description,
            price: service[0].price,
            locationName: doc.data().location === "1" ? "Acoyapa" : "Juigalpa",
            state: doc.data().isactive === "1" ? "Activo" : "Inactivo",
            ...doc.data()
        });
    });
    return list;
}

export const getAgreeByClient = async (user) => {
    let list = [];
    const q = query(collection(db, AGREES_KEY), where("user", "==", user));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc) => {
        list.push({
            id: doc.id,
            ...doc.data()
        });
    });
    return list[0];
}

export const createAgreement = async (agreement) => {
    await addDoc(collection(db, AGREES_KEY), { ...agreement });
}

export const updateAgreement = async (agreement, id) => {
    await updateDoc(doc(db, AGREES_KEY, id), { ...agreement });
}

export const deleteAgreement = async (id) =>{
    await deleteDoc(doc(db, AGREES_KEY, id));
}
