import { db } from '../firebase';
import { format } from 'date-fns';
import { getAgreements } from './agreement';
import { collection, addDoc, deleteDoc, doc, query, where, getDocs, updateDoc } from 'firebase/firestore';

const BREAK_KEY = 'Breakdowns';

export const getBreakdowns = async () => {
    let list = [];
    const querySnapshot = await getDocs(collection(db, BREAK_KEY));
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            documentId: doc.id,
            createdAt: format(doc.data().created.toDate(), 'dd/MM/yyyy'),
            ...doc.data()
        });
    });
    return list;
}

export const getBreakStats = async () => {
    const data = await getBreakdowns();
    return [
        ["Reportada", data.filter(x => x.state === 1).length],
        ["En proceso", data.filter(x => x.state === 2).length],
        ["Reparada", data.filter(x => x.state === 3).length],
    ]
}

export const getBreaksByClient = async (client) => {
    const { id, name, lastName } = client.agree;
    let list = [];
    const q = query(collection(db, BREAK_KEY), where("agreement", "==", id));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            number: id,
            client: `${name} ${lastName}`,
            documentId: doc.id,
            createdAt: format(doc.data().created.toDate(), 'dd/MM/yyyy'),
            ...doc.data()
        });
    });
    return list;
}

export const getBreaksWithClients = async () => {
    const clients = await getAgreements();
    const breaks = await getBreakdowns();
    let list = [];
    breaks.forEach((item) => {
        let newItem = item;
        let client = clients.filter(x => x.number === item.agreement)[0];
        newItem.client = `${client.name} ${client.lastName}`;
        newItem.number = client.number;
        newItem.address = client.address;
        list.push(newItem);
    });
    return list;
}

export const createBreakdown = async (down) => {
    await addDoc(collection(db, BREAK_KEY), { ...down, created: new Date(), state: 1 });
}

export const updateBreakdown = async (down, id) => {
    await updateDoc(doc(db, BREAK_KEY, id), { ...down });
}

export const deleteBreakdown = async (id) => {
    await deleteDoc(doc(db, BREAK_KEY, id));
}
