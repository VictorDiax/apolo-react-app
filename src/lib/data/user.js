import { db } from '../firebase';
import { format } from 'date-fns';
import {
    getAuth, signInWithEmailAndPassword,
    createUserWithEmailAndPassword, onAuthStateChanged,
    signOut
} from 'firebase/auth';
import { collection, query, setDoc, doc, getDocs, getDoc } from 'firebase/firestore';
import { getAgreements, updateAgreement, getAgreeByClient } from './agreement';

const USER_KEY = 'Users';
const auth = getAuth();

export const queryUser = (userId) => {
    return doc(db, USER_KEY, userId);
};

export const selectUser = async (userId) => {
    const current = JSON.parse(localStorage.getItem('current'));
    if (current === null) {
        const document = current === null ? await getDoc(queryUser(userId)) : current;
        if (document.exists) {
            const agree = document.data().type === 2 ? await getAgreeByClient(document.id) : [];
            localStorage.setItem('current', JSON.stringify({
                id: document.id,
                band: document.data().type,
                agree
            }));
            return JSON.parse(localStorage.getItem('current'));
        } else return null;
    }
    else return current;
};

export const updateAuth = async (email, password, id) => {
    console.log('loading update function...')
};

export const currentUser = () => {
    const user = auth.currentUser;
    if (user) {
        return user;
    } else {
        return null;
    }
};

export const signIn = async (email, password) => {
    await signInWithEmailAndPassword(auth, email, password);
};

export const createUser = async (user) => {
    const { email, password, type, agree } = user;
    const result = await createUserWithEmailAndPassword(auth, email, password);
    updateAgreement({ user: result.user.uid }, agree);
    await setDoc(doc(db, USER_KEY, result.user.uid), { email, password, type, created: new Date() });
    return result.user.uid;
};


export const getUsers = async () => {
    let list = [];
    const q = query(collection(db, USER_KEY));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.forEach((doc, key) => {
        list.push({
            id: key + 1,
            createdAt: format(doc.data().created.toDate(), 'dd/MM/yyyy'),
            documentId: doc.id,
            ...doc.data()
        });
    });
    return list;
}

export const getClientsWithUsers = async () => {
    const clients = await getAgreements();
    const users = await getUsers();
    let list = [];
    clients.forEach((item) => {
        let newItem = item;
        let user = users.filter(x => x.documentId === item.user);
        newItem.email = user.length > 0 ? user[0].email : '';
        newItem.userCreated = user.createdAt;
        newItem.hasUser = user.length > 0 ? true : false;
        list.push(newItem);
    });
    return list;
}

export const handleSignOut = async () => {
    localStorage.removeItem('current');
    await signOut(auth);
};

export const onAuthChanged = (response) => {
    onAuthStateChanged(auth, response);
};