import { db } from '../firebase';
import { format } from 'date-fns';
import { collection, addDoc, deleteDoc, doc, getDocs, updateDoc } from 'firebase/firestore';

const SERVS_KEY = 'Services';

export const getServices = async () => {
    let list = [];
    const querySnapshot = await getDocs(collection(db, SERVS_KEY));
    querySnapshot.docs.forEach((doc) => {
        list.push({
            id: doc.id,
            ...doc.data()
        });
    });
    return list;
}

export const paymentDate = async () => {
    const data = await getServices();
    const d = new Date();
    return `${data[0].day}/${format(d, 'M')}/${d.getFullYear()}`;
}

export const createService = async (service) => {
    await addDoc(collection(db, SERVS_KEY), { created: new Date(), ...service });
}

export const updateService = async (service, id) => {
    await updateDoc(doc(db, SERVS_KEY, id), { ...service });
}

export const deleteService = async (id) => {
    await deleteDoc(doc(db, SERVS_KEY, id));
}
