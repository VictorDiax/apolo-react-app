import 'firebase/firestore';
import 'firebase/compat/auth';
import 'firebase/auth';
import 'firebase/storage';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBJSN0M5x7tGci3PpU5YQsc2v3hD4wv1hU",
    authDomain: "apolo-9cf6f.firebaseapp.com",
    projectId: "apolo-9cf6f",
    storageBucket: "apolo-9cf6f.appspot.com",
    messagingSenderId: "731950134682",
    appId: "1:731950134682:web:466087e3c1726522e5f623",
    measurementId: "G-K8MCZG0QYX"
};

const firebase = initializeApp(firebaseConfig);
const db = getFirestore();

export { firebase, db };
