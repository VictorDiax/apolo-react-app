import { combineReducers } from 'redux';
import { authReducer } from './auth';

const root = combineReducers({
    auth: authReducer
});

export default root;